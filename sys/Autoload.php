<?php

function __autoload($className) {
    if (in_array($className, ['DataBase', 'ModelInterface', 'Controller', 'Misc', 'Session', 'AdminController'])) {
        require_once 'sys/' . $className . '.php';
    } elseif (preg_match('/^([A-Z][a-z]+)+Controller$/', $className)) {
        require_once 'app/controllers/' . $className . '.php';
    } elseif (preg_match('/^([A-Z][a-z]+)+Model$/', $className)) {
        require_once 'app/models/' . $className . '.php';
    } elseif ($className === 'Configuration') {
        require_once 'Configuration.php';
    }
}
