<?php

final class Session {

    public final static function begin() {
        session_start();
    }

    public static final function end() {
        self::clear();
        session_destroy();
    }

    public final static function clear() {
        $_SESSION = [];
        return true;
    }

    public final static function set($key, $value) {
        if (self::isValid($key)) {
            $_SESSION[$key] = $value;
            return true;
        } else {
            return false;
        }
    }

    public final static function getKey($key) {
        if (self::exists($key) && self::isValid($key)) {
            return $_SESSION[$key];
        } else {
            return false;
        }
    }
    
    public final static function exists($key) {
        if (self::isValid($key)) {
            return isset($_SESSION[$key]);
        } else {
            return false;
        }
    }

    private final static function isValid($key) {
        return preg_match('/^[A-z][A-z0-9_]*$/', $key);
    }


}
