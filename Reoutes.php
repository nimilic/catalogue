<?php

return [
    [   # Pocetak admin rute za admin panel.
        'Pattern' => '|^login/?$|',
        'Controller' => 'Main',
        'Method' => 'login'
    ],
    [
        'Pattern' => '|^logout/?$|',
        'Controller' => 'Main',
        'Method' => 'logout'
    ],
    [
        'Pattern' => '|^admin/([a-z0-9\-]+)/?$|',
        'Controller' => 'Main',
        'Method' => 'home'
    ],  # Kraj admin rute za admin panel.
    [   # Admin rute za proizvode.
        'Pattern' => '|^product/list/?$|',
        'Controller' => 'AdminProduct',
        'Method' => 'index'
    ],
    [
        'Pattern' => '|^product/add/?$|',
        'Controller' => 'AdminProduct',
        'Method' => 'add'
    ],
    [
        'Pattern' => '|^product/edit/([0-9]+)/?$|',
        'Controller' => 'AdminProduct',
        'Method' => 'edit'
    ],
    [
        'Pattern' => '|^product/delete/([0-9]+)/?$|',
        'Controller' => 'AdminProduct',
        'Method' => 'delete'
    ], # Kraj admin ruta za proizvode.    
    [  # Admin rute za kategorije.
        'Pattern' => '|^category/list/?$|',
        'Controller' => 'AdminCategory',
        'Method' => 'index'
    ],
    [  
        'Pattern' => '|^category/add/?$|',
        'Controller' => 'AdminCategory',
        'Method' => 'add'
    ],
    [
        'Pattern' => '|^category/edit/([0-9]+)/?$|',
        'Controller' => 'AdminCategory',
        'Method' => 'edit'
    ], # Kraj admin ruta za kategorije.
    [  # Admin rute za pakovanje.
        'Pattern' => '|^packing/list/?$|',
        'Controller' => 'AdminPacking',
        'Method' => 'index'
    ],
    [
        'Pattern' => '|^packing/add/?$|',
        'Controller' => 'AdminPacking',
        'Method' => 'add'
    ],
    [
        'Pattern' => '|^packing/edit/([0-9]+)/?$|',
        'Controller' => 'AdminPacking',
        'Method' => 'edit'
    ], # Kraj admin ruta za pakovanje.     
    [  # Admin rute za proizvodjaca.
        'Pattern' => '|^producer/list/?$|',
        'Controller' => 'AdminProducer',
        'Method' => 'index'
    ],
    [
        'Pattern' => '|^producer/add/?$|',
        'Controller' => 'AdminProducer',
        'Method' => 'add'
    ],
    [
        'Pattern' => '|^producer/edit/([0-9]+)/?$|',
        'Controller' => 'AdminProducer',
        'Method' => 'edit'
    ], # Kraj admin ruta za proizvodjaca.     
    [  # Admin rute za upravljane adminima.
        'Pattern' => '|^admins/list/?$|',
        'Controller' => 'AdminAdmin',
        'Method' => 'index'
    ],
    [
        'Pattern' => '|^admins/add/?$|',
        'Controller' => 'AdminAdmin',
        'Method' => 'add'
    ],
    [
        'Pattern' => '|^admins/edit/([0-9]+)/?$|',
        'Controller' => 'AdminAdmin',
        'Method' => 'edit'
    ], # Kraj admin rute za upravljane adminima.     
    [  # Admin rute za upravljane tagovima.
        'Pattern' => '|^tag/list/?$|',
        'Controller' => 'AdminTag',
        'Method' => 'index'
    ],
    [
        'Pattern' => '|^tag/add/?$|',
        'Controller' => 'AdminTag',
        'Method' => 'add'
    ],
    [
        'Pattern' => '|^tag/edit/([0-9]+)/?$|',
        'Controller' => 'AdminTag',
        'Method' => 'edit'
    ], # Kraj admin rute za upravljane adminima.     
    [  # Admin rute za upravljane slikama.
        'Pattern' => '|^images/product/([0-9]+)/?$|',
        'Controller' => 'AdminImage',
        'Method' => 'listProductImages'
    ],
    [
        'Pattern' => '|^images/product/([0-9]+)/add/?$|',
        'Controller' => 'AdminImage',
        'Method' => 'uploadImage'
    ],  # Kraj admin rute za upravljane slikama. 
    [   #Ruta za prikaz kategorija.
        'Pattern' => '|^category/([a-z0-9\-]+)/?$|', 
        'Controller' => 'Product',
        'Method' => 'listByCategory'
    ],  # Kraj rute za prikaz kategorija.
    [   # Ruta za prikaz proizvoda.
        'Pattern' => '|^products/?$|',
        'Controller' => 'Product',
        'Method' => 'index'
    ],
    [   
        'Pattern' => '|^products/([0-9]+)/?$|',
        'Controller' => 'Product',
        'Method' => 'index'
    ],
    [
        'Pattern' => '|^product/([a-z0-9\-]+)/?$|', #ovo je moguc proble...
        'Controller' => 'Product',
        'Method' => 'details'
    ],  # Kraj rute za prikaz prozivoda.
    [   # Ruta za prikaz starne o nama.
        'Pattern' => '|^page/error/?$|',
        'Controller' => 'Page',
        'Method' => 'error'
    ],
    [
        'Pattern' => '|^page/([a-z0-9\-]+)/?$|',
        'Controller' => 'Page',
        'Method' => 'show'
    ],  # Kraj rute za prikaz starne o nama.
    [   # Ruta za upravljane kontakt stranu. 
        'Pattern' => '|^contact/?$|',
        'Controller' => 'Contact',
        'Method' => 'index'
    ],
    [
        'Pattern' => '|^contact/send/?$|',
        'Controller' => 'Contact',
        'Method' => 'handle'
    ],  # Kraj rute kontakt stranu.
    [   # Ruta za upravljane mapom. 
        'Pattern' => '|^found-us/?$|',
        'Controller' => 'Maps',
        'Method' => 'index'
    ],  # Kraj rute za upravljanje mapom.
    [   # Ovo je poslednja ruta!
        'Pattern' => '|^.*$|',
        'Controller' => 'Main',
        'Method' => 'index'
    ]
];
