/*
Navicat MySQL Data Transfer

Source Server         : nikola
Source Server Version : 50505
Source Host           : localhost:3306
Source Database       : ecatalog

Target Server Type    : MYSQL
Target Server Version : 50505
File Encoding         : 65001

Date: 2016-05-14 02:35:29
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for admin
-- ----------------------------
DROP TABLE IF EXISTS `admin`;
CREATE TABLE `admin` (
  `admin_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(64) NOT NULL,
  `password_hash` char(128) NOT NULL,
  `active` tinyint(4) NOT NULL DEFAULT '1',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`admin_id`),
  UNIQUE KEY `username` (`username`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of admin
-- ----------------------------
INSERT INTO `admin` VALUES ('1', 'nikola', '90cc2e1b8b27308a4f75976730f96bfacce05047c83f92672f412b5c0a795e9aee47bb611281a2cb16ab0b933b860a1c5e969d6fc5faa53791742bc32afe36a9', '1', '2016-05-09 00:20:52');
INSERT INTO `admin` VALUES ('2', 'stefan', '90cc2e1b8b27308a4f75976730f96bfacce05047c83f92672f412b5c0a795e9aee47bb611281a2cb16ab0b933b860a1c5e969d6fc5faa53791742bc32afe36a9', '1', '2016-05-09 00:20:56');
INSERT INTO `admin` VALUES ('24', 'jovana', 'e728699baf5a59be58b2aa06b1922b477b90ffc2e5eccc912bee8070f6614890473d9ca77ad10d7f7bf9484cd9826ad8df54f4f2e8e42cefff2237e96602df91', '1', '2016-05-12 12:41:05');

-- ----------------------------
-- Table structure for admin_login
-- ----------------------------
DROP TABLE IF EXISTS `admin_login`;
CREATE TABLE `admin_login` (
  `admin_login_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `admin_id` int(11) unsigned NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `ip` mediumtext NOT NULL,
  PRIMARY KEY (`admin_login_id`),
  KEY `fk_admin_login_1` (`admin_id`),
  CONSTRAINT `fk_admin_login_1` FOREIGN KEY (`admin_id`) REFERENCES `admin` (`admin_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of admin_login
-- ----------------------------

-- ----------------------------
-- Table structure for images
-- ----------------------------
DROP TABLE IF EXISTS `images`;
CREATE TABLE `images` (
  `image_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `path` varchar(255) NOT NULL,
  `datetime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`image_id`),
  UNIQUE KEY `path_UNIQUE` (`path`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of images
-- ----------------------------

-- ----------------------------
-- Table structure for message
-- ----------------------------
DROP TABLE IF EXISTS `message`;
CREATE TABLE `message` (
  `message_id` int(11) NOT NULL AUTO_INCREMENT,
  `datetime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `subject` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `text` text COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`message_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of message
-- ----------------------------
INSERT INTO `message` VALUES ('1', '2016-03-07 12:27:38', 'mtair@singidunum.ac.rs', 'Moja prva poruka', 'Lorem ipsum dolorem ist namet...');
INSERT INTO `message` VALUES ('2', '2016-03-09 14:18:15', 'mtair@singidunum.ac.rs', 'Moja prva poruka', 'sdgsdfg');
INSERT INTO `message` VALUES ('3', '2016-03-14 11:41:59', 'mtair@singidunum.ac.rs', 'dfgdfged', 'yrtyrty');
INSERT INTO `message` VALUES ('4', '2016-04-26 20:19:01', 'nikolla.cilim@gmail.com', 'Pitanje', 'Imam jedno pitanje u vez...');

-- ----------------------------
-- Table structure for page
-- ----------------------------
DROP TABLE IF EXISTS `page`;
CREATE TABLE `page` (
  `page_id` int(11) NOT NULL AUTO_INCREMENT,
  `seo_url` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `seo_title` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `title` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `content` text COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`page_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of page
-- ----------------------------
INSERT INTO `page` VALUES ('1', 'about-us', 'About us | My website', 'About us', '<p>Donec non sapien felis. Quisque sodales congue nulla, at bibendum elit elementum vel. Sed tincidunt varius odio id tincidunt. Sed nulla augue, molestie ac volutpat et, cursus a metus. Donec lobortis tortor eu dignissim posuere.</p>\r\n<p>Integer non erat lacus. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Curabitur sagittis sem magna, sit amet dignissim metus auctor id. Maecenas id posuere mauris.</p>\r\n<p>Cras quis enim dignissim, feugiat enim pretium, convallis risus. Sed at fermentum turpis. Nam bibendum ipsum ut lectus tempus pharetra. Aenean ut pellentesque dolor.</p>');
INSERT INTO `page` VALUES ('2', 'our-portfolio', 'Portfolio | My website', 'Portfolio of our work', '<p>Vivamus et bibendum leo. Nulla ullamcorper magna nec erat luctus, eget vestibulum felis condimentum. Phasellus venenatis <b>lorem ipsum</b>, quis malesuada massa tempus id. Etiam ut justo dui.</p>\r\n<p>Maecenas vel ligula magna. Nam pretium orci quis risus maximus, id consectetur velit egestas. Nam sed semper augue, vitae fringilla ante. Morbi efficitur risus eget lorem rhoncus, ac efficitur urna porttitor.</p>');

-- ----------------------------
-- Table structure for producer_product
-- ----------------------------
DROP TABLE IF EXISTS `producer_product`;
CREATE TABLE `producer_product` (
  `producer_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `description` text NOT NULL,
  PRIMARY KEY (`producer_id`),
  UNIQUE KEY `name_UNIQUE` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of producer_product
-- ----------------------------
INSERT INTO `producer_product` VALUES ('1', 'rojal kaninn', 'ovo je neki opis..');
INSERT INTO `producer_product` VALUES ('2', 'cats', 'da');

-- ----------------------------
-- Table structure for product
-- ----------------------------
DROP TABLE IF EXISTS `product`;
CREATE TABLE `product` (
  `product_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(64) NOT NULL,
  `short_description` mediumtext NOT NULL,
  `long_description` text CHARACTER SET ascii NOT NULL,
  `price` decimal(10,2) unsigned NOT NULL,
  `producer_id` int(11) unsigned NOT NULL,
  `admin_id` int(11) unsigned NOT NULL,
  `active` enum('no','yes') NOT NULL DEFAULT 'yes',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`product_id`),
  KEY `fk_product_1` (`admin_id`),
  KEY `fk_product_2_idx` (`producer_id`),
  CONSTRAINT `fk_product_1` FOREIGN KEY (`admin_id`) REFERENCES `admin` (`admin_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_product_2` FOREIGN KEY (`producer_id`) REFERENCES `producer_product` (`producer_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of product
-- ----------------------------

-- ----------------------------
-- Table structure for product_category
-- ----------------------------
DROP TABLE IF EXISTS `product_category`;
CREATE TABLE `product_category` (
  `product_category_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(64) NOT NULL,
  `visible` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`product_category_id`),
  UNIQUE KEY `name_UNIQUE` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of product_category
-- ----------------------------
INSERT INTO `product_category` VALUES ('1', 'Psi', '1');
INSERT INTO `product_category` VALUES ('2', 'Macke', '1');
INSERT INTO `product_category` VALUES ('3', 'Ptice', '1');
INSERT INTO `product_category` VALUES ('4', 'Reptilicic', '1');

-- ----------------------------
-- Table structure for product_packing
-- ----------------------------
DROP TABLE IF EXISTS `product_packing`;
CREATE TABLE `product_packing` (
  `packing_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(64) NOT NULL,
  `visible` tinyint(4) NOT NULL DEFAULT '1',
  PRIMARY KEY (`packing_id`),
  UNIQUE KEY `name` (`name`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of product_packing
-- ----------------------------
INSERT INTO `product_packing` VALUES ('1', 'teglicno', '1');
INSERT INTO `product_packing` VALUES ('2', 'komadno', '1');

-- ----------------------------
-- Table structure for product_packing_product
-- ----------------------------
DROP TABLE IF EXISTS `product_packing_product`;
CREATE TABLE `product_packing_product` (
  `product_id` int(11) unsigned NOT NULL,
  `packing_id` int(11) unsigned NOT NULL,
  PRIMARY KEY (`product_id`,`packing_id`),
  KEY `fk_product_packing_product_product1_idx` (`product_id`),
  KEY `fk_product_packing_product_product_packing1_idx` (`packing_id`),
  CONSTRAINT `fk_packing_1` FOREIGN KEY (`product_id`) REFERENCES `product` (`product_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_packing_2` FOREIGN KEY (`packing_id`) REFERENCES `product_packing` (`packing_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of product_packing_product
-- ----------------------------

-- ----------------------------
-- Table structure for product_product_category
-- ----------------------------
DROP TABLE IF EXISTS `product_product_category`;
CREATE TABLE `product_product_category` (
  `product_category_id` int(11) unsigned NOT NULL,
  `product_id` int(11) unsigned NOT NULL,
  PRIMARY KEY (`product_category_id`,`product_id`),
  KEY `fk_product_category_1_idx` (`product_category_id`),
  KEY `fk_product_product_category_product1_idx` (`product_id`),
  CONSTRAINT `fk_category_1` FOREIGN KEY (`product_category_id`) REFERENCES `product_category` (`product_category_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_category_product_2` FOREIGN KEY (`product_id`) REFERENCES `product` (`product_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of product_product_category
-- ----------------------------

-- ----------------------------
-- Table structure for product_product_tag
-- ----------------------------
DROP TABLE IF EXISTS `product_product_tag`;
CREATE TABLE `product_product_tag` (
  `product_tag_id` int(11) unsigned NOT NULL,
  `product_id` int(11) unsigned NOT NULL,
  PRIMARY KEY (`product_tag_id`,`product_id`),
  KEY `fk_product_product_tag_product_tag1_idx` (`product_tag_id`),
  KEY `fk_product_product_tag_product1_idx` (`product_id`),
  CONSTRAINT `fk_=product_tag_1` FOREIGN KEY (`product_id`) REFERENCES `product` (`product_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_product_tag_2` FOREIGN KEY (`product_tag_id`) REFERENCES `product_tag` (`product_tag_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of product_product_tag
-- ----------------------------

-- ----------------------------
-- Table structure for product_tag
-- ----------------------------
DROP TABLE IF EXISTS `product_tag`;
CREATE TABLE `product_tag` (
  `product_tag_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(64) NOT NULL,
  `image_class` varchar(128) NOT NULL,
  PRIMARY KEY (`product_tag_id`),
  UNIQUE KEY `name` (`name`),
  UNIQUE KEY `image_class_UNIQUE` (`image_class`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of product_tag
-- ----------------------------
INSERT INTO `product_tag` VALUES ('1', 'psi', 'hrana-psi');
