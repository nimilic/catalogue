<?php

final class Configuration {

    const DB_HOST = 'localhost';
    const DB_NAME = 'ecatalog';
    const DB_USER = 'nikola';
    const DB_PASS = 'nikola';
    
    const BASE_URL = 'http://localhost/webcatalogue/';
    const WEB_PATH = '/webcatalogue/'; 
    
    const SALT = 'por4s8ys6hddf5jlp6i8776s354dsf545ku87p08d36a';
    
    const IMAGE_PATH = 'assets/product/images';
    
    const ITEMS_PER_PAGE = 12; #Broj artikla koji ce biti prikazani po stranici.
    
    const ITEMS_NEW_PER_PAGE = 5; #Broj artikla koji ce biti prikazani po stranici.
    
}
