<?php require_once 'app/views/global_frame/header.php'; ?>

<article class="block">
    <header class="subject">
        <h1>Dobrodošli,<?php foreach ($DATA['admins'] as $admin): ?>
                <?php
                $name = $admin;
                $length = strlen($admin);
                $part = substr($name, $length - 1);
                if ($part == 'n' || $part == 's') {
                    $addition = ucfirst($name . 'e');
                    echo $addition;
                } else {
                    echo ucfirst($name);
                }
                ?>
            <?php endforeach; ?>
        </h1>
    </header>
    <p><?php echo $DATA['page']->content; ?></p>
    <nav id="home">
        <ul>
            <li>
                <a href="<?php echo Configuration::BASE_URL; ?>product/list/">            
                    <img src="<?php echo Configuration::BASE_URL; ?>assets/images/navicons/product.png" alt="Lista proizvoda" title="Lista proizvoda">                   
                </a>
            </li>     
            <li>
                <a href="<?php echo Configuration::BASE_URL; ?>category/list/"> 
                    <img src="<?php echo Configuration::BASE_URL; ?>assets/images/navicons/categories.png" alt="Kategorije" title="Kategorije proizvoda"> 
                </a>
            </li>
            <li>
                <a href="<?php echo Configuration::BASE_URL; ?>tag/list/"> 
                    <img src="<?php echo Configuration::BASE_URL; ?>assets/images/navicons/productag.png" alt="Tagovi" title="Lista tagova">
                </a>
            </li>
            <li>
                <a href="<?php echo Configuration::BASE_URL; ?>packing/list/"> 
                    <img src="<?php echo Configuration::BASE_URL; ?>assets/images/navicons/packing.png" alt="Pakovanje" title="Pakovanja proizvoda">
                </a>
            </li>
            <li>
                <a href="<?php echo Configuration::BASE_URL; ?>producer/list/"> 
                    <img src="<?php echo Configuration::BASE_URL; ?>assets/images/navicons/producers.png" alt="Proizvođači" title="Lista proizvođača">
                </a>
            </li>

            <li>
                <a href="<?php echo Configuration::BASE_URL; ?>admins/list/"> 
                    <img src="<?php echo Configuration::BASE_URL; ?>assets/images/navicons/admins.png" alt="Proizvođači" title="Lista administratora">
                </a>
            </li>
        </ul>
    </nav>
</article>

<?php require_once 'app/views/global_frame/footer.php'; ?>

