<?php require_once 'app/views/global_frame/header.php'; ?>

<article class="block">
    <!-- Jssor Slider Begin -->
    <div id="slider1_container" class="slider1_container-a">
        <!-- Loading Screen --> 
        <div u="loading" class="loading-screen">
            <div class="loading-u"></div> 
            <div class="loading"></div> 
        </div> 
        <!-- Slides Container --> 
        <div u="slides" class="slides-container">
            <div>
                <img u="image" src="assets/images/slider/cat-slider.jpg" />
                <div u=caption t="CLIP|LR" du="1500" class="captionOrange captionOrange-a"> 
                    Da li ste se negde uputili i već odlazite?
                </div>
                <div u=caption t="L|IB" t2=L d=-900 class="captionBlack captionBlack-b">Sačekajte...</div> 
                <div u=caption t="RTT|360" d=-200 class="captionBlack captionBlack-v">Možda?</div>
                <div u=caption t="DDGDANCE|RB" t2="RTT|10" d=-1800 du=3800 class="captionOrange captionOrange-g">
                    &nbsp; Možemo<br>
                    &nbsp; Da odgovorimo na<br>
                    &nbsp; Sva Vaša pitanja<br>
                    &nbsp; Istražite našu stranicu.
                </div>
                <div u=caption t="T|IE*IE" d=-1600 du=3800 t2="B" class="captionOrange captionOrange-d">
                    U našem asortimanu nudimo preko
                </div>
                <div u="caption" t="ZMF|10" t2="B" d=-1300 class="captionBlack number">250</div>
                <div u="caption" t="CLIP|L" d=-300 class="captionOrange number-product">artikala.</div>
            </div>
            <div> 
                <img u="image" src="assets/images/slider/dog-cat-slider.jpg"/>               
                <div u="caption" t="FADE" t2="B" d=-450 class="captionBlack captionBlack-e">
                    Imate li macu<br>
                    kucu?..<br>
                    Ili nekog drugog
                </div>
                <div u="caption" t="T|IB" t2="R" d=-600 class="captionOrange captionOrange-z">ljubimca?</div>
                <div u="caption" t="MCLIP|T" t2="T" d=-450 class="no-caption-a">Da li vam je potrebna?</div>									
                <div u="caption" t="MCLIP|R" d=-380 class="no-caption-b">Hrana za odrasle</div>									
                <div u="caption" t="MCLIP|R" d=-380 class="no-caption-c">ili male životinje?</div>									
                <div u="caption" t="MCLIP|R" d=-380 class="captionBlack captionBlack-a">Ogrlice?</div>								
                <div u="caption" t="MCLIP|R" d=-380 class="captionOrange captionOrange-b">
                    Ili druga oprema za vašeg omiljenog ljubimca?
                </div>
            </div>
            <div> 
                <img u="image" src="assets/images/slider/animals-slider.jpg">
                <div u=caption t="T" t2=NO class="captionOrange captionOrange-v"> 
                    Ptice,<br>
                    Akvaristika,<br>
                    Teratristika.
                </div>
                <div u=caption t="L" d=-750 class="captionBlack captionBlack-g">U vašoj prodavnici</div>
                <div u=caption t="CLIP|L" t2=B d=-450 class="captionOrange captionOrange-e">možete </div>
                <div u=caption t="DDG|TR" t2="TORTUOUS|VB" d=-750 class="captionBlack captionBlack-z">kupiti kaveze, akvarijume</div>
                <div u=caption t="RTT|10" d=-450 class="captionBlack captionBlack-l">za</div>
                <div u=caption t="TORTUOUS|VB" d=-750 class="captionOrange captionOrange-lj">ptice</div>
                <div u=caption t="T" d=-450 class="captionBlack captionBlack-i"> ribice</div>
                <div u=caption t="FLTTR|R" t2="B" d=-600 class="captionOrange captionOrange-i"> zečeve</div>
                <div u=caption t="ATTACK|BR" d=-600 class="captionOrange captionOrange-nj"> kornjače</div>
                <div u="caption" t="FLTTRWN|LT" d=-900 class="captionBlack captionBlack-dj">glodare</div>
            </div>
            <div>
                <img u="image" src="assets/images/slider/fish-slider.jpg">
                <div u=caption t="RTTS|T" d=-380 t2="B" class="captionOrange captionOrange-o"> 
                    Izrađujemo akvarijume po vašoj želji
                </div>
                <div u=caption t="T|IB" t2="T" d=-380 class="captionOrange captionOrange-p">Iskustvo</div>
                <div u=caption t="T|IB" t2=L d=-900 class="captionOrange captionOrange-j">Kvalitet</div>
                <div u=caption t="TR" t2="T" d=-900 class="captionOrange captionOrange-k">Saveti</div>									
                <div u=caption t="R" t2=R d=-900 class="captionBlack captionBlack-d">Oprema</div>								
                <div u="caption" t="T|IB" d=-900 class="captionBlack captionBlack-m">Hrana</div>
            </div>
            <div u="any" class="icons">
                <a class="share-icon share-facebook" target="_blank" href="https://www.facebook.com/PET-SHOP-Kolibri-833237580076972/?fref=ts" title="Share on Facebook"></a>
                <a class="share-icon share-twitter" target="_blank" href="http://twitter.com/share?url=http://jssor.com&text=JavaScript%20jQuery%20Image%20Slider/Slideshow/Carousel/Gallery/Banner%20html%20TOUCH%20SWIPE%20Responsive" title="Share on Twitter"></a>
                <a class="share-icon share-googleplus" target="_blank" href="https://plus.google.com/share?url=http://jssor.com" title="Share on Google Plus"></a>
                <a class="share-icon share-linkedin" target="_blank" href="http://www.linkedin.com/shareArticle?mini=true&url=http://jssor.com" title="Share on LinkedIn"></a>
                <a class="share-icon share-stumbleupon" target="_blank" href="http://www.stumbleupon.com/submit?url=http://jssor.com&title=JavaScript%20jQuery%20Image%20Slider/Slideshow/Carousel/Gallery/Banner%20html%20TOUCH%20SWIPE%20Responsive" title="Share on StumbleUpon"></a>
                <a class="share-icon share-pinterest" target="_blank" href="http://pinterest.com/pin/create/button/?url=http://jssor.com&media=http://jssor.com/img/site/jssor.slider.jpg&description=JavaScript%20jQuery%20Image%20Slider/Slideshow/Carousel/Gallery/Banner%20html%20TOUCH%20SWIPE%20Responsive" title="Share on Pinterst"></a>
                <a class="share-icon share-email" target="_blank" href="mailto:?Subject=Jssor%20Slider&Body=Highly%20recommended%20JavaScript%20jQuery%20Image%20Slider/Slideshow/Carousel/Gallery/Banner%20html%20TOUCH%20SWIPE%20Responsive%20http://jssor.com" title="Share by Email"></a>
            </div>         
        </div> 
        <!-- bullet navigator container -->
        <div u="navigator" class="jssorb03" style="bottom: 16px; right: 6px;">
            <!-- bullet navigator item prototype -->
            <div u="prototype"><div u="numbertemplate"></div></div>
        </div>
        <!-- Arrow Left -->
        <span u="arrowleft" class="jssora20l" style="top: 123px; left: 8px;">
        </span>
        <!-- Arrow Right -->
        <span u="arrowright" class="jssora20r" style="top: 123px; right: 8px;">
        </span>
    </div>
    <!-- Jssor Slider End -->
    <header class="subject">
        <h1>Najnoviji proizvodi u nasem asortimanu</h1>
    </header> 
    <div class="product-home">
        <?php foreach ($DATA['products'] as $product): ?>
            <?php require 'app/views/global_frame/product_file.php'; ?>           
        <?php endforeach; ?>
    </div>
</article>

<?php require_once 'app/views/global_frame/footer.php'; ?>