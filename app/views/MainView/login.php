<?php require_once 'app/views/global_frame/header.php'; ?>

<article class="block">
    <header class="subject">
        <h1>Dobrodošao na stranicu za administatora!</h1>
    </header>
    <p><?php echo $DATA['text']; ?></p>
    <form method="post">
        <div class="form">
            <label id="f1_username">Korisničko ime:</label>
            <input type="text" name="username" id="f1_username" pattern="^[a-z0-9]{4,}$" class="input-field" required>
        </div>
        <div class="form">
            <label id="f1_password">Lozinka:</label>
            <input type="password" name="password" id="f1_password" pattern="^.{6,}$" class="input-field" required>
        </div>
        <div class="form">
            <button type="submit" class="button">Prijavi se</button>    
        </div>
    </form>
    <?php if(isset($DATA['message'])): ?>
        <p><?php echo htmlspecialchars($DATA['message']); ?></p>
    <?php endif; ?>
</article>

<?php require_once 'app/views/global_frame/footer.php'; ?>