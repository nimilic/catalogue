<?php require_once 'app/views/global_frame/header.php'; ?>

<article class="block">
    <header class="subject">
        <h1><?php echo $DATA['page']->title; ?></h1> 
    </header>   
    <div class="page">
        <div id="image">
            <img alt="slika_o_nama" src="<?php echo Configuration::BASE_URL; ?>assets/images/colibri_o_nama.jpg">
        </div>
        <div class="description">
            <p><?php echo $DATA['page']->content; ?></p>
        </div>
    </div>
</article>

<?php require_once 'app/views/global_frame/footer.php'; ?>