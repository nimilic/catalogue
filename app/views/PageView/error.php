<?php require_once 'app/views/global_frame/header.php'; ?>

<article class="block">
    <header class="subject">
        <h1><?php echo $DATA['title']; ?></h1>
    </header> 
        <p><?php echo $DATA['description']; ?></p>
</article>

<?php require_once 'app/views/global_frame/footer.php'; ?>