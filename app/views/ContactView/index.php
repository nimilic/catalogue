<?php require_once 'app/views/global_frame/header.php'; ?>

<article class="block">
    <header class="subject">
        <h1>Ostanimo u kontaktu</h1>
    </header>
    <form method="post" action="contact/send">
        <div class="form">
            <label for="f1_email">E-mail adresa:</label>
            <input type="email" name="email" id="f1_email" class="input-field" placeholder="e-mail adresa..." required><br>
        </div>
        <div class="form">
            <label for="f1_subject">Naslov poruke:</label>
            <input type="text" name="subject" id="f1_subject" class="input-field" placeholder="naslov poruke..." required><br>
        </div>
        <div class="form">
            <label for="f1_message">Vaša poruka:</label>
            <textarea name="message" rows="10" id="f1_message" class="input-field" placeholder="tekst poruke..." required></textarea><br>
        </div>
        <div class="form">
            <label class="hide-on-mobile"></label>
        <button type="submit" class="button">Pošalji</button>   
    </form>

</article>

<?php require_once 'app/views/global_frame/footer.php'; ?>