<!doctype html>
<html>
    <head>
        <title><?php echo @$DATA['seo_title']; ?></title>
        <link href="<?php echo Configuration::BASE_URL; ?>assets/images/colibri_favicon.png" rel="icon" type="image/png"/>
        <link href="https://fonts.googleapis.com/css?family=Dancing+Script" rel="stylesheet">
        <link href="http://fonts.googleapis.com/css?family=Lobster|Great+Vibes|Courgette|Playball" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Merriweather:400,700,700italic" rel="stylesheet">
        <link href="<?php echo Configuration::BASE_URL; ?>assets/css/mains.css" rel="stylesheet"/>
        <link href="<?php echo Configuration::BASE_URL; ?>assets/css/<?php echo $FoundRoute['Controller']; ?>.css" rel="stylesheet">     
        <link href="<?php echo Configuration::BASE_URL; ?>assets/css/mobile.css" rel="stylesheet">
        <link href="<?php echo Configuration::BASE_URL; ?>assets/css/<?php echo $FoundRoute['Controller']; ?>-mobile.css" rel="stylesheet">
        <meta charset="UTF-8">
    </head>
    <body>
        <section id="wrapper">
            <div id="header">
                <a href="index.php" id="logo">
                    <img alt="Kolibri logo" src="<?php echo Configuration::BASE_URL; ?>assets/images/colibri_logo.png">
                </a>
                <header id="name">
                    <h1>Pet Shop Kolibri</h1>
                </header>
            </div>  
            <nav id="nav_menu">
                <ul>  
                    <?php if (!Session::exists('admin_id')) : ?>
                        <li><?php Misc::url('', 'Početna'); ?></li> 
                        <li><?php Misc::url('products', 'Proizvodi'); ?></li>
                        <li><?php Misc::url('page/about-us', 'O nama'); ?></li> 
                        <li><?php Misc::url('contact', 'Kontakt'); ?></li>
                        <li><?php Misc::url('found-us', 'Mapa'); ?></li>
                        <li><?php Misc::url('login', 'Prijavi se?'); ?></li>
                <?php endif; ?>
                        <?php if (Session::exists('admin_id')) : ?>
                        <li class="admin-nav"><?php Misc::url('logout', 'Odjava?'); ?></li>  
                    <?php endif; ?>
                </ul>
            </nav>    
            <main id="max-mini-height">