
<div class="products">
    <div class="picture-list">
        <a href="<?php echo Configuration::BASE_URL; ?>product/<?php echo $product->slug; ?>">
            <img src="<?php echo Configuration::BASE_URL . $product->images[0]->path; ?>" alt="<?php echo htmlspecialchars($product->name); ?>" class="image">
        </a>
    </div>
    <div class="subject-list">
        <a href="<?php echo Configuration::BASE_URL; ?>product/<?php echo $product->slug; ?>">
            <header class="name_product">
                <h1><?php echo $product->name; ?></h1>                
            </header>
        </a>
    </div>
</div>

