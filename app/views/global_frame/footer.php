                </main>     
            <footer id="footer">
                &COPY; <?php echo date('Y'); ?> - Pet Shop Kolibri
            </footer>  
        </section>  
        <script src="<?php echo Configuration::BASE_URL; ?>assets/js/jquery-1.9.1.min.js"></script>
        <script src="<?php echo Configuration::BASE_URL; ?>assets/js/jssor.js"></script>
        <script src="<?php echo Configuration::BASE_URL; ?>assets/js/jssor.slider.js"></script>
        <script src="<?php echo Configuration::BASE_URL; ?>assets/js/jquery/jquery-slider.js"></script>
    </body>  
</html>
