<?php require_once 'app/views/global_frame/header.php'; ?>

<article class="block">
    <header class="subject">
        <h1>Proizvod pripada kategoriji: &quot;<?php echo htmlspecialchars($DATA['product']->categories->name); ?>&quot;</h1>
    </header>
    <section class="wrapper">
        <div class="images-product">
            <?php foreach ($DATA['product']->images as $image): ?>
                <img  src="<?php echo Configuration::BASE_URL . $image->path; ?>" alt="<?php echo $DATA['product']->name; ?>">
            <?php endforeach; ?>
            <div class="producer">
                <h4>Proizvođač:&thinsp;<b><?php echo htmlspecialchars($DATA['product']->producer->name); ?></b></h4>
            </div> 
        </div>     
        <div class="product-subject">
            <header id="subject-p">
                <h2><?php echo $DATA['product']->name; ?></h2>
            </header>
            <header id="about-product">
                <h3>O proizvodu:</h3>
            </header>
            <div class="product-short-description">
                <p><?php echo $DATA['product']->short_description; ?></p>
            </div>
            <div id="packing">
                <h4>Pakovanje:</h4>
                <?php foreach ($DATA['product']->packings as $packing): ?>
                    <ul class="packing-list">
                        <li><?php echo $packing->name; ?></li>
                    </ul>
                <?php endforeach; ?>     
            </div>
            <div class="price">
                <span><strong>Cena: <?php echo $DATA['product']->price; ?> din.</strong></span>
            </div>
            <div class="product-long-description">
                <section>
                    <header>
                        <h4>Specifikacija proizvoda:</h4>
                    </header>
                    <p><?php echo $DATA['product']->long_description; ?></p>
                </section>
            </div>
        </div>
    </section>
</article>

<?php require_once 'app/views/global_frame/footer.php'; ?>