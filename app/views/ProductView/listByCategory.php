<?php require_once 'app/views/global_frame/header.php'; ?>

<article class="block">
    <header class="subject">
        <h1>Proizvodi iz kategorije: &quot;<?php echo htmlspecialchars($DATA['category']->name); ?> &quot;</h1>
    </header> 
    <div class="category">
        <div id="side-bar">
        <nav class="nav-category">
            <ul>
                <?php foreach ($DATA['categories'] as $category): ?>
                <li>
                    <?php Misc::url('category/' . $category->slug, $category->name); ?>
                </li>
                <?php endforeach; ?>
            </ul>
        </nav>
        </div>
        <div class="product">
            <?php foreach ($DATA['products'] as $product): ?>
                <?php require 'app/views/global_frame/product_file.php'; ?>           
            <?php endforeach; ?>
        </div>
    </div> 
</article>

<?php require_once 'app/views/global_frame/footer.php'; ?>