<?php require_once 'app/views/global_frame/header.php'; ?>

<article class="block">
    <header class="subject">
        <h1 id="admin-subject">Izmeni kategoriju: &quot;<?php echo htmlspecialchars($DATA['category']->name); ?>&quot;</h1>
    </header>
    <div class="link">
        <div class="link-back">
            <a href="<?php echo Configuration::BASE_URL; ?>category/list"> << nazad</a>      
        </div>
    </div>
    <form method="post" action="<?php echo Configuration::BASE_URL; ?>category/edit/<?php echo $DATA['category']->product_category_id; ?>">
        <div class="form">
            <label for="f1_name">Ime kategorije:</label>
            <input type="text" name="name" id="f1_name" class="input-field" pattern="^([A-Z][A-z\ ]+)$" value="<?php echo htmlspecialchars($DATA['category']->name); ?>" required><br>
        </div> 
        <div class="form">
            <label class="hide-on-mobile"></label>
            <button type="submit" class="button">Ažuriraj kategoriju</button>   
        </div>
    </form>

    <?php if (isset($DATA['message'])): ?>
        <p><?php echo htmlspecialchars($DATA['message']); ?></p>
    <?php endif; ?>

</article>

<?php require_once 'app/views/global_frame/footer.php'; ?>