<?php require_once 'app/views/global_frame/header.php'; ?>

<article class="block">
    <header class="subject">
        <h1 id="admin-subject">Lista kategorija</h1>
    </header>
    <div class="link">
        <div class="link-back">
            <a href="<?php echo Configuration::BASE_URL; ?>admin/home"> << nazad</a>      
        </div>
        <div class="link-add">
            <a href="<?php echo Configuration::BASE_URL; ?>category/add">+ Kreiraj kategoriju</a>      
        </div>
    </div>
    <table class="table-small">
        <thead>
            <tr>
                <th>Id</th>
                <th>Naziv</th>
                <th>Slug</th>
                <th>Opcija</th>
            </tr>
        </thead>
        <tbody>
            <?php
            $broj = 1;
            while ($broj <= count($DATA['categories'])):
                foreach ($DATA['categories'] as $category):
                    ?>
                    <tr>
                        <td><?php echo $broj++; ?></td>
                        <td><?php echo $category->name; ?></td>
                        <td><?php echo $category->slug; ?></td>
                        <td><?php Misc::url('category/edit/' . $category->product_category_id, 'Izmeni'); ?></td>
                    </tr>
                <?php
                endforeach;
            endwhile;
            ?>
        </tbody>    
    </table>
</article>

<?php require_once 'app/views/global_frame/footer.php'; ?>