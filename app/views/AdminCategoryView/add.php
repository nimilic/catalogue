<?php require_once 'app/views/global_frame/header.php'; ?>

<article class="block">
    <header class="subject">
        <h1 id="admin-subject">Dodaj novu kategoriju</h1>
    </header>
    <div class="link">
        <div class="link-back">
            <a href="<?php echo Configuration::BASE_URL; ?>category/list"> << nazad</a>      
        </div>
    </div>
    <form method="post" action="<?php echo Configuration::BASE_URL; ?>category/add/">
        <div class="form">
            <label for="f1_name">Ime kategorije:</label>
            <input type="text" name="name" id="f1_name" pattern="^[A-Z][a-z\ ]+$" class="input-field" required><br>
        </div>
        <div class="form">
            <label class="hide-on-mobile"></label>
            <button type="submit" class="button">Dodaj kategoriju</button>   
        </div>
    </form>

    <?php if (isset($DATA['message'])): ?>
        <p><?php echo htmlspecialchars($DATA['message']); ?></p>
    <?php endif; ?>

</article>

<?php require_once 'app/views/global_frame/footer.php'; ?>
