<?php require_once 'app/views/global_frame/header.php'; ?>

<article class="block">
    <header class="subject">
        <h1 id="admin-subject">Izmena proizvođača: &nbsp;<?php echo htmlspecialchars($DATA['produser']->name); ?> &nbsp;</h1>
    </header>
    <div class="link">
        <div class="link-back">
            <a href="<?php echo Configuration::BASE_URL; ?>producer/list"> << nazad</a>      
        </div>
    </div>
    <form method="post" action="<?php echo Configuration::BASE_URL; ?>producer/edit/<?php echo $DATA['produser']->producer_id; ?>">
        <div class="form">
            <label for="f1_producer">Izmeni proizvođača:</label>
            <input type="text" name="producer" id="f1_producer" value="<?php echo htmlspecialchars($DATA['produser']->name); ?>" class="input-field" pattern="^[A-Z][A-z\ ]+$" required>
        </div>
        <div class="form">
            <label for="f1_description">Promeni opis proizvođača:</label>
            <textarea name="description" id="f1_description" rows="10" class="input-field" required><?php echo htmlspecialchars($DATA['produser']->description); ?></textarea>
        </div>

        <div class="form">
            <label class="hide-on-mobile"></label>
            <button type="submit" class="button">Izmeni</button>   
        </div>
    </form>

    <?php if (isset($DATA['message'])): ?>
        <p><?php echo htmlspecialchars($DATA['message']); ?></p>
    <?php endif; ?>
</article>

<?php require_once 'app/views/global_frame/footer.php'; ?>