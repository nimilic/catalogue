<?php require_once 'app/views/global_frame/header.php'; ?>

<article class="block">
    <header class="subject">
        <h1 id="admin-subject">Lista proizvođača</h1>
    </header>
    <div class="link">
        <div class="link-back">
            <a href="<?php echo Configuration::BASE_URL; ?>admin/home"> << nazad</a>      
        </div>
        <div class="link-add">
            <a class="link" href="<?php echo Configuration::BASE_URL; ?>producer/add"> + Kreiraj proizvođača</a>    
        </div>
    </div>
    <table class="table-small">
        <thead>
            <tr>
                <th>Id</th>
                <th>Ime proizvođača</th>
                <th>Opcije</th>
            </tr>
        </thead>
        <tbody>
            <?php
            $broj = 1;
            while ($broj <= count($DATA['producers'])):
                foreach ($DATA['producers'] as $producer):
                    ?>
                    <tr>
                        <td><?php echo $broj++; ?></td>
                        <td><?php echo $producer->name; ?></td>
                        <td><?php Misc::url('producer/edit/' . $producer->producer_id, 'Izmeni'); ?></td>
                    </tr>
                    <?php
                endforeach;
            endwhile;
            ?>
        </tbody>    
    </table> 
</article>

<?php require_once 'app/views/global_frame/footer.php'; ?>