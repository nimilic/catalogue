<?php require_once 'app/views/global_frame/header.php'; ?>

<article class="block">
    <header class="subject">
        <h1 id="admin-subject">Dodaj novog proizvođača</h1>
    </header>
    <div class="link">
        <div class="link-back">
            <a href="<?php echo Configuration::BASE_URL; ?>producer/list"> << nazad</a>      
        </div>
    </div>
    <form method="post" action="<?php echo Configuration::BASE_URL; ?>producer/add/">
        <div class="form">
            <label for="f1_producer">Ime proizvođača:</label>
            <input type="text" name="producer" id="f1_producer" pattern="^[A-Z][A-z\ ]+$" class="input-field" required>
        </div>
        <div class="form">  
            <label for="f1_description">Opis proizvođača:</label>
            <textarea name="description" id="f1_description" rows="10" class="input-field" required></textarea>
        </div>

        <div class="form">
            <label class="hide-on-mobile"></label>
            <button type="submit" class="button">Dodaj proizvođača</button>   
        </div>
    </form>

    <?php if (isset($DATA['message'])): ?>
        <p><?php echo htmlspecialchars($DATA['message']); ?></p>
    <?php endif; ?>
</article>

<?php require_once 'app/views/global_frame/footer.php'; ?>