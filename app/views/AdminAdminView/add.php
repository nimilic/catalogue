<?php require_once 'app/views/global_frame/header.php'; ?>

<article class="block">
    <header class="subject">
        <h1 id="admin-subject">Dodaj novog administratora</h1>
    </header>
    <div class="link">
        <div class="link-back">
            <a href="<?php echo Configuration::BASE_URL; ?>admins/list"> << nazad</a>      
        </div>
    </div>
    <form method="post" action="<?php echo Configuration::BASE_URL; ?>admins/add/">
        <div class="form">
            <label for="f1_username">Korisničko ime:</label>
            <input type="text" name="username" id="f1_username" pattern="^[a-z0-9]{4,}$" class="input-field" required>
        </div>
        <div class="form">
            <label for="f1_password">Lozinka:</label>
            <input type="password" name="password_hash" id="f1_password" pattern="^.{6,}$" class="input-field" required>
        </div>
        <div class="form">
            <label class="hide-on-mobile"></label>
            <button type="submit" class="button">Kreiraj admina</button>   
        </div>        
    </form>
    <?php if (isset($DATA['message'])): ?>
        <p><?php echo htmlspecialchars($DATA['message']); ?></p>
    <?php endif; ?>
</article>

<?php require_once 'app/views/global_frame/footer.php'; ?>