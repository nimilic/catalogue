<?php require_once 'app/views/global_frame/header.php'; ?>

<article class="block">
    <header class="subject">
        <h1 id="admin-subject">Lista administratora</h1>
    </header>
    <div class="link">
        <div class="link-back">
            <a href="<?php echo Configuration::BASE_URL; ?>admin/home"> << nazad</a>      
        </div>
        <div class="link-add">
            <a href="<?php echo Configuration::BASE_URL; ?>admins/add">+ Novi administrator</a> 
        </div>
    </div>
    <table class="table">
        <thead>
            <tr>
                <th>Id</th>
                <th>Ime</th>
                <th>Opcije</th>
            </tr>
        </thead>
        <tbody>
            <?php
            $broj = 1;
            while ($broj <= count($DATA['admins'])):
                foreach ($DATA['admins'] as $admin):
                    ?>
                    <tr>
                        <td><?php echo $broj++; ?></td>
                        <td><?php echo $admin->username; ?></td>

                        <td><?php Misc::url('admins/edit/' . $admin->admin_id, 'Izmeni'); ?></td>
                    <?php endforeach;
                endwhile;
                ?>
        </tbody>    
    </table>  
</article>

<?php require_once 'app/views/global_frame/footer.php'; ?>