<?php require_once 'app/views/global_frame/header.php'; ?>

<article class="block">
    <header class="subject">
        <h1 id="admin-subject">Izmeni podatke o administratoru: &quot;<?php echo htmlspecialchars($DATA['admin']->username); ?>&quot;</h1>
    </header>
    <div class="link">
        <div class="link-back">
            <a href="<?php echo Configuration::BASE_URL; ?>admins/list"> << nazad</a>      
        </div>
    </div>
    <form method="post" action="<?php echo Configuration::BASE_URL; ?>admins/edit<?php echo $DATA['admin']->admin_id; ?>">
        <div class="form">
            <label for="f1_username">Korisničko ime:</label>
            <input type="text" name="username" id="f1_username" pattern="^[a-z0-9]{4,}$" value="<?php echo htmlspecialchars($DATA['admin']->username); ?>" class="input-field" required>
        </div>
        <div class="form">
            <label for="f1_password">Nova lozinka:</label>
            <input type="password" name="password_hash" id="f1_password" value="<?php echo htmlspecialchars($DATA['category']->password_hash); ?>" pattern="^[a-z0-9]{4,}$" class="input-field" required>
        </div>
        <div class="form">
            <label class="hide-on-mobile"></label>
            <button type="submit" class="button">Izmeni podatke</button>   
        </div> 

    </form>
    <?php if (isset($DATA['message'])): ?>
        <p><?php echo htmlspecialchars($DATA['message']); ?></p>
    <?php endif; ?>
</article>

<?php require_once 'app/views/global_frame/footer.php'; ?>