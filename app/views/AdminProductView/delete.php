<?php require_once 'app/views/global_frame/header.php'; ?>

<article class="block">
    <header class="subject">
        <h1>Brisanje proizvoda: &quot;<?php echo htmlspecialchars($DATA['product']->name); ?>&quot;?</h1>
    </header>
    <form method="post" action="<?php echo Configuration::BASE_URL; ?>product/delete/<?php echo $DATA['product']->product_id; ?>">
        <input type="hidden" name="confirmed" value="1">    
        <button type="submit" class="button">Obriši proizvod</button>   
        <a class="button" href="<?php echo Configuration::BASE_URL; ?>product/list">Vrati se na listu</a>
    </form>

    <?php if (isset($DATA['message'])): ?>
        <p><?php echo htmlspecialchars($DATA['message']); ?></p>
    <?php endif; ?>

</article>

<?php require_once 'app/views/global_frame/footer.php'; ?>