<?php require_once 'app/views/global_frame/header.php'; ?>

<article class="block">
    <header class="subject">
        <h1 id="admin-subject">Izmena proizvoda: &quot;<?php echo htmlspecialchars($DATA['product']->name); ?>&quot;</h1>
    </header>
    <div class="link">
        <div class="link-back">
            <a href="<?php echo Configuration::BASE_URL; ?>product/list"> << nazad</a>      
        </div>
    </div>
    <form method="post" action="<?php echo Configuration::BASE_URL; ?>product/edit/<?php echo $DATA['product']->product_id; ?>">
        <div class="form">
            <label for="f1_name">Ime proizvoda::</label>
            <input type="text" name="name" id="f1_name" class="input-field" required value="<?php echo htmlspecialchars($DATA['product']->name); ?>"><br>
        </div>
        <div class="form">
            <label for="f1_description">Kratak opis:</label>
            <textarea name="short_description" rows="5" id="f1_description" class="input-field" required><?php echo htmlspecialchars($DATA['product']->short_description); ?></textarea>
        </div>
        <div class="form">
            <label for="f1_description">Duži opis:</label>
            <textarea name="long_description" rows="10" id="f1_description" class="input-field" required><?php echo htmlspecialchars($DATA['product']->long_description); ?></textarea>
        </div>
        <div class="form">
            <label for="f1_price">Cena:</label>
            <input type="number" min="0.01" step="any" name="price" id="f1_price" class="input-field" required value="<?php echo intval($DATA['product']->price); ?>">
        </div>  
        <div class="form">
            <label for="f1_category">Kategorija:</label>
            <select name="category_id" id="f1_category">
                <?php foreach ($DATA['categories'] as $category): ?> 
                    <option value="<?php echo $category->product_category_id; ?>" <?php if ($DATA['product']->product_category_id == $category->product_category_id) echo 'selected'; ?>>
                        <?php echo htmlspecialchars($category->name); ?>
                    </option>
                <?php endforeach; ?>
            </select>
        </div>              
        <div class="form">
            <label>Tip pakovanja:</label>
            <?php foreach ($DATA['packing'] as $pack): ?> 
                <input type="checkbox" name="packings_id[]" value="<?php echo $pack->packing_id; ?>"
                       <?php if (in_array($pack->packing_id, $DATA['product']->packings_id)) echo 'checked'; ?>>
                       <?php echo htmlspecialchars($pack->name); ?>
                   <?php endforeach; ?>
        </div>     
        <div class="form">
            <label>Tag proizvoda:</label>
            <?php foreach ($DATA['tags'] as $tag): ?> 
                <input type="checkbox" name="tags_id[]" value="<?php echo $tag->product_tag_id; ?>"
                       <?php if (in_array($tag->product_tag_id, $DATA['product']->tags_id)) echo 'checked'; ?>>             
                       <?php echo htmlspecialchars($tag->name); ?>
                   <?php endforeach; ?>
        </div>                  
        <div class="form">
            <label for="f1_producer">Proizvođač:</label>
            <select name="producer_id" id="f1_producer">
                <?php foreach ($DATA['producers'] as $producer): ?>
                    <option value="<?php echo $producer->producer_id; ?>" <?php if ($DATA['product']->producer_id == $producer->producer_id) echo 'selected'; ?>>
                        <?php echo htmlspecialchars($producer->name); ?>
                    </option>
                <?php endforeach; ?>
            </select>
        </div>   
        <div class="form">
            <label class="hide-on-mobile"></label>
            <button type="submit" class="button">Save new values</button>   
        </div>
    </form>
    <?php if (isset($DATA['message'])): ?>
        <p><?php echo htmlspecialchars($DATA['message']); ?></p>
    <?php endif; ?>

</article>

<?php require_once 'app/views/global_frame/footer.php'; ?>