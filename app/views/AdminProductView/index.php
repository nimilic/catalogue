<?php require_once 'app/views/global_frame/header.php'; ?>

<article class="block">
    <header class="subject">
        <h1 id="admin-subject">Lista proizvoda</h1>
    </header>
    <div class="link">
        <div class="link-back">
            <a href="<?php echo Configuration::BASE_URL; ?>admin/home"> << nazad</a>      
        </div>
        <div class="link-add">
            <a class="link" href="<?php echo Configuration::BASE_URL; ?>product/add"> + Novi proizvod</a>  
        </div>
    </div>
    <table class="table">
        <thead>
            <tr>
                <th>ID</th>
                <th>Ime</th>
                <th>Kategorija</th>
                <th>Pakovanje</th>
<!--                <th>Tag proizvoda</th>-->
                <th>Cena</th>
                <th colspan="3">Opcije</th>
            </tr>
        </thead>
        <tbody>
            <?php
            $broj = 1;
            while ($broj <= count($DATA['products'])):
                foreach ($DATA['products'] as $item):
                    ?>           
                    <tr>
                        <td><?php echo $broj++; ?></td>
                        <td><?php echo htmlspecialchars($item->name); ?></td>
                        <td>
                            <ul>
                                <?php
                                foreach ($DATA['categories'] as $category):
                                    if ($item->product_category_id == $category->product_category_id):
                                        ?>                              
                                        <li><?php echo $category->name; ?></li>
                                        <?php
                                    endif;
                                endforeach;
                                ?>
                            </ul>
                        </td>
                        <td>
                            <ul>
                                <?php foreach ($item->packing as $productPacking): ?>
                                    <li><?php echo htmlspecialchars($productPacking->name); ?></li>
                                <?php endforeach; ?>
                            </ul>
                        </td>
<!--                        <td>
                            <ul>
                                <?php //foreach ($item->tags as $productTag): ?>
                                    <li><?php// echo htmlspecialchars($productTag->name); ?></li>
                                <?php //endforeach; ?>
                            </ul>
                        </td>-->
                        <td><?php echo $item->price; ?>$</td>
                        <td><?php Misc::url('product/edit/' . $item->product_id, 'Izmeni') ?></td>
                        <td><?php Misc::url('product/delete/' . $item->product_id, 'Obriši') ?></td>
                        <td><?php Misc::url('images/product/' . $item->product_id, 'Slike') ?></td>
                    </tr>
                    <?php
                endforeach;
            endwhile;
            ?>
        </tbody>
    </table>
</article>

<?php require_once 'app/views/global_frame/footer.php'; ?>