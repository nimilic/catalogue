<?php require_once 'app/views/global_frame/header.php'; ?>

<article class="block">
    <header class="subject">
        <h1 id="admin-subject">Lista tagova</h1>
    </header>
    <div class="link">
        <div class="link-back">
            <a href="<?php echo Configuration::BASE_URL; ?>admin/home"> << nazad</a>      
        </div>
        <div class="link-add">
            <a class="button" href="<?php echo Configuration::BASE_URL; ?>tag/add">+ Kreiraj tag</a>      
        </div>
    </div>
    <table class="table-small">
        <thead>
            <tr>
                <th>Id</th>
                <th>Ime taga</th>
                <th>Klasa taga</th>
                <th>Opcije</th>
            </tr>
        </thead>
        <tbody>
            <?php
            $broj = 1;
            while ($broj <= count($DATA['tags'])):
                foreach ($DATA['tags'] as $tag):
                    ?>
                    <tr>
                        <td><?php echo $broj++; ?></td>
                        <td><?php echo $tag->name; ?></td>
                        <td><?php echo $tag->image_class; ?></td>
                        <td><?php Misc::url('tag/edit/' . $tag->product_tag_id, 'Izmeni'); ?></td>
                    <?php endforeach;
                endwhile;
                ?>
        </tbody>    
    </table>  
</article>

<?php require_once 'app/views/global_frame/footer.php'; ?>