<?php require_once 'app/views/global_frame/header.php'; ?>

<article class="block">
    <header class="subject">
        <h1 id="admin-subject">Izmeni tag: &quot;<?php echo htmlspecialchars($DATA['tags']->name); ?>&quot;</h1>
    </header>
    <div class="link">
        <div class="link-back">
            <a href="<?php echo Configuration::BASE_URL; ?>tag/list"> << nazad</a>      
        </div>
    </div>
    <form method="post" action="<?php echo Configuration::BASE_URL; ?>tag/edit/<?php echo $DATA['tags']->product_tag_id; ?>">
        <div class="form">
            <label for="f1_tag">Ime taga:</label>
            <input type="text" name="name" id="f1_tag" value="<?php echo htmlspecialchars($DATA['tags']->name); ?>" class="input-field" required><br>
        </div>
        <div class="form">
            <label for="f1_image_class">Klasa taga:</label>
            <input type="text" name="image_class" id="f1_image_class" value="<?php echo htmlspecialchars($DATA['tags']->image_class); ?>" pattern="^[a-z0-9\- ]+$"  class="input-field" required><br>
        </div>

        <div class="form">
            <label class="hide-on-mobile"></label>
            <button type="submit" class="button">Izmeni tag</button>   
        </div>
    </form>
    <?php if (isset($DATA['message'])): ?>
        <p><?php echo htmlspecialchars($DATA['message']); ?></p>
    <?php endif; ?>   
</article>

<?php require_once 'app/views/global_frame/footer.php'; ?>
