<?php require_once 'app/views/global_frame/header.php'; ?>

<article class="block">
    <header class="subject">
        <h1 id="admin-subject">Lista Fotografija</h1>
    </header>
    <div class="link">
        <div class="link-back">
            <a href="<?php echo Configuration::BASE_URL; ?>product/list"> << nazad</a>     
        </div>
        <div class="link-add">
            <a href="<?php echo Configuration::BASE_URL . 'images/product/' . $DATA['product_id'] . '/add/'; ?>">+ Nova fotografija</a>  
        </div>
    </div>    
    <table class="table">
        <thead>
            <tr>
                <th>Id</th>
                <th>Image</th>
                <th>Opcija</th>
            </tr>
        </thead>
        <tbody>
            <?php
            $broj = 1;
            while ($broj <= count($DATA['images'])):
                foreach ($DATA['images'] as $image):
                    ?>
                    <tr>
                        <td><?php echo $broj++; ?></td>
                        <td>
                            <img src="<?php echo Configuration::BASE_URL . htmlspecialchars($image->path); ?>" class="small-image">                                                      
                        </td>                      
                        <td><?php Misc::url('images//product/' . $DATA['product_id'] . '/delete/' . $image->image_id, 'Obrisi'); ?></td>
                    </tr>
                    <?php
                endforeach;
            endwhile;
            ?>
        </tbody>    
    </table>
</article>

<?php require_once 'app/views/global_frame/footer.php'; ?>