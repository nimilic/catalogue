<?php require_once 'app/views/global_frame/header.php'; ?>

<article class="block">
    <header class="subject">
        <h1 id="admin-subject">Dodaj novu sliku</h1>
    </header>
    <div class="link">
        <div class="link-back">
            <a href="<?php echo Configuration::BASE_URL; ?>product/list"> << nazad</a>      
        </div>
    </div>
    <form method="post" enctype="multipart/form-data">
        <div class="form">
            <label for="f1_image">Izaberite sliku:</label>
            <input type="file" name="image" id="f1_image" required><br>
        </div>
        <div class="form">
            <label class="hide-on-mobile"></label>
            <button type="submit" class="button">Dodaj sliku</button>   
        </div>
    </form>
    <?php if (isset($DATA['message'])): ?>
        <p><?php echo htmlspecialchars($DATA['message']); ?></p>
    <?php endif; ?>

</article>

<?php require_once 'app/views/global_frame/footer.php'; ?>
