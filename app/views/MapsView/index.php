<?php require_once 'app/views/global_frame/header.php'; ?>

<article class="block">
    <header class="subject">
        <h1><?php echo $DATA['subtitle']; ?></h1>
    </header>
        <p><?php echo $DATA['description']; ?></p>
    <table>
        <tbody>
            <tr>
                <td><?php echo $DATA['workday'] ?></td>
            </tr>
            <tr>
                <td><?php echo $DATA['weekend'] ?></td>
            </tr>
        </tbody>
    </table>
    <div class="background">
        <header class="subtitle">
            <h2><?php echo $DATA['subject']; ?></h2>
        </header>
        <div class="mapa">
            <p><?php echo $DATA['maps']; ?></p>
        </div>
        <div class="flexible-container">
            <iframe src="https://www.google.com/maps/d/u/0/embed?mid=zKqK8wEpW5E4.k9Hd1Uof3iHU" width="640" height="480"></iframe>
        </div>
    </div>
</article>

<?php require_once 'app/views/global_frame/footer.php'; ?>