<?php require_once 'app/views/global_frame/header.php'; ?>

<article class="block">
    <header class="subject">
        <h1 id="admin-subject">Izmeni pakovanje: &quot;<?php echo htmlspecialchars($DATA['packing']->name); ?>&quot;</h1>
    </header>
    <div class="link">
        <div class="link-back">
            <a href="<?php echo Configuration::BASE_URL; ?>packing/list"> << nazad</a>      
        </div>
    </div>
    <form method="post" action="<?php echo Configuration::BASE_URL; ?>packing/edit/<?php echo $DATA['packing']->packing_id; ?>">
        <div class="form">
            <label for="f1_packing">Tip pakovanja:</label>
            <input type="text" name="packing" id="f1_packing" pattern="[a-z]([\ ?[a-z]+)" class="input-field" value="<?php echo htmlspecialchars($DATA['packing']->name); ?>" required><br>
        </div>
        <div class="form">
            <label class="hide-on-mobile"></label>
            <button type="submit" class="button">Ažuriraj pakovanje</button>   
        </div>
    </form>  
    <?php if (isset($DATA['message'])): ?>
        <p><?php echo htmlspecialchars($DATA['message']); ?></p>
    <?php endif; ?>  
</article>

<?php require_once 'app/views/global_frame/footer.php'; ?>


