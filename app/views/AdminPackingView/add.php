<?php require_once 'app/views/global_frame/header.php'; ?>

<article class="block">
    <header class="subject">
        <h1 id="admin-subject">Dodaj novo pakovanje</h1>
    </header>
    <div class="link">
        <div class="link-back">
            <a href="<?php echo Configuration::BASE_URL; ?>packing/list"> << nazad</a>      
        </div>
    </div>
    <form method="post" action="<?php echo Configuration::BASE_URL; ?>packing/add/">
        <div class="form">
            <label for="f1_packing">Ime pakovanja:</label>
            <input type="text" name="packing" id="f1_packing" pattern="[a-z]([\ ?[a-z]+)" class="input-field" required><br>
        </div>
        <div class="form">
            <label class="hide-on-mobile"></label>
            <button type="submit" class="button">Dodaj pakovanje</button>   
        </div>
    </form>

    <?php if (isset($DATA['message'])): ?>
        <p><?php echo htmlspecialchars($DATA['message']); ?></p>
    <?php endif; ?>

</article>

<?php require_once 'app/views/global_frame/footer.php'; ?>
