<?php require_once 'app/views/global_frame/header.php'; ?>

<article class="block">
    <header class="subject">
        <h1 id="admin-subject">Lista pakovanja</h1>
    </header>
    <div class="link">
        <div class="link-back">
            <a href="<?php echo Configuration::BASE_URL; ?>admin/home"> << nazad</a>      
        </div>
        <div class="link-add">
            <a href="<?php echo Configuration::BASE_URL; ?>packing/add">+ Kreiraj pakovanje</a>    
        </div>
    </div>
    <table class="table-small">
        <thead>
            <tr>
                <th>Id</th>
                <th>Naziv</th>
                <th>Prikazano</th>
                <th>Opcija</th>
            </tr>
        </thead>
        <tbody>    
            <?php
            $broj = 1;
            while ($broj <= count($DATA['packing'])):
                foreach (($DATA['packing']) as $pack):
                    ?>
                    <tr>
                        <td><?php echo $broj++; ?></td>               
                        <td><?php echo $pack->name; ?></td> 
                        <td><?php echo $pack->visible; ?></td> 
                        <td><?php Misc::url('packing/edit/' . $pack->packing_id, 'Izmeni'); ?></td>
                    </tr>  
                    <?php
                endforeach;
            endwhile;
            ?>
        </tbody>
    </table>  
</article>

<?php require_once 'app/views/global_frame/footer.php'; ?>