<?php

/*
 * Klasa admin koja se koristi za manipulaciju administatorima.
 */

class AdminModel implements ModelInterface {

    /**
     * Metoda koja vraca spisak svih administratora sortiranih po korisnickom imenu.
     * @return array
     */
    public static function getAll() {
        $SQL = 'SELECT * FROM `admin` ORDER By `username`;';
        $prep = DataBase::getInstance()->prepare($SQL);
        $prep->execute();
        return $prep->fetchAll(PDO::FETCH_OBJ);
    }

    /**
     * Metoda koja vraca objekat sa podacima administratora
     * ciji je admin_id, dat kao argument metoda.
     * @param int $admin_id
     * @return stdClass|NULL
     */
    public static function getById($admin_id) {
        $admin_id = intval($admin_id);
        $SQL = 'SELECT * FROM `admin` WHERE `admin_id` = ?;';
        $prep = DataBase::getInstance()->prepare($SQL);
        $prep->execute([$admin_id]);
        return $prep->fetch(PDO::FETCH_OBJ);
    }

    /**
     * Metoda preko koje se dodaju novi administratori u tabelu `admin`.
     * @param string $username
     * @param string $password_hash
     * @return int|bolean -> Ako je zapis uspesno kreiran vratice ID zapisa, a u slucaju da nije
     *                       vratice false. (Dodavanje nije uspelo)
     */
    public static function add($username, $password_hash) {
        $SQL = 'INSERT INTO `admin` (`username`, `password_hash`) VALUES (?, ?);';
        $prep = DataBase::getInstance()->prepare($SQL);
        $res = $prep->execute([$username, $password_hash]);
        if ($res) {
            $id = DataBase::getInstance()->lastInsertId();
            return $id;
        } else {
            return false;
        }
    }

    /**
     * Metoda koja se koristi za izmenu postojeceg svojstva objekta `admin_id`,
     * gde su kao argumenti funkcije prosledjeni:
     * @param int $admin_id
     * @param string $username
     * @param string $password_hash
     * @return boolean   
     * @todo Potrebno je implementirati nacin za uzimanje zapisa iz baze koji se odnosi na
     *       na lozinku i da se omoguci azuriranje iste.
     */
    public static function edit($admin_id, $username, $password_hash) {
        $admin_id = intval($admin_id);
        $SQL = 'UPDATE `admin` SET `username` = ?, `password_hash` = ? WHERE `admin_id` = ?;';
        $prep = DataBase::getInstance()->prepare($SQL);
        return $prep->execute([$username, $password_hash, $admin_id]);
    }

}
