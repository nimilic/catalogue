<?php

/**
 * Klasa koja je namenjena za rad sa podacima iz tabele `product_category`.
 */
class CategoryModel implements ModelInterface {

    /**
     * Metoda koja vraca spisak svih kategorija, poredjanih po imenu.
     * @return array (Object)
     */
    public static function getAll() {
        $SQL = 'SELECT * FROM `product_category`;';
        $prep = DataBase::getInstance()->prepare($SQL);
        $prep->execute();
        return $prep->fetchAll(PDO::FETCH_OBJ);
    }

    /**
     * Metoda koja vraca objekat sa podacima kategorije ciji je `product_category_id`,
     * dat kao argument metoda.
     * @param int $product_category_id
     * @return stdClass|NULL
     */
    public static function getById($product_category_id) {
        $product_category_id = intval($product_category_id);
        $SQL = 'SELECT * FROM `product_category` WHERE `product_category_id` = ?;';
        $prep = DataBase::getInstance()->prepare($SQL);
        $prep->execute([$product_category_id]);
        return $prep->fetch(PDO::FETCH_OBJ);
    }

    /**
     * Metoda preko koje se dodaju novi administratori u tabelu `product_category`.
     * @param string $name
     * @return int|boolean -> Ako je zapis uspesno kreiran vratice ID zapisa, a u slucaju da nije
     *                       vratice false. (Dodavanje nije uspelo)
     * @todo Potrebno je implementrirati dodatak, koji ce se odnositi na prikazivanje i sakrivanje
     * odredjene kategorije. (visible)
     */
    public static function add($name, $slug) {
        $SQL = 'INSERT INTO `product_category` (`name`, `slug`) VALUES (?, ?);';
        $prep = DataBase::getInstance()->prepare($SQL);
        $res = $prep->execute([$name, $slug]);
        if ($res) {
            $id = DataBase::getInstance()->lastInsertId();
            return $id;
        } else {
            return false;
        }
    }

    /**
     * Metoda koja se koristi za izmenu postojeceg svojstva objekta `product_category_id`,
     * gde su kao argumenti funkcije prosledjeni:
     * @param int $product_category_id
     * @param string $name
     * @return boolean 
     * @todo Potrebno je implementrirati dodatak, koji ce se odnositi na prikazivanje i sakrivanje
     *       odredjene kategorije. (visible)    
     */
    public static function edit($product_category_id, $name, $slug) {
        $product_category_id = intval($product_category_id);
        $SQL = 'UPDATE `product_category` SET `name` = ?, `slug` = ? WHERE `product_category_id` = ?;';
        $prep = DataBase::getInstance()->prepare($SQL);
        return $prep->execute([$name, $slug, $product_category_id]);
    }
    
    /**
     * Metoda koja vraca objekat sa podacima proizvoda ciji je `slug`, dat kao argument metoda.
     * @param int $slug
     * @return stdClass|NULL
     */
    public static function getCategoryBySlug($slug){
        $SQL = 'SELECT * FROM `product_category` WHERE `slug` = ?;';
        $prep = DataBase::getInstance()->prepare($SQL);
        $prep->execute([$slug]);
        return $prep->fetch(PDO::FETCH_OBJ);
    }
    
}
