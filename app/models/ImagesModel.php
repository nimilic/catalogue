<?php

/**
 * Model koji je namenjen za rad sa slikama iz tabele `images`.
 */
class ImagesModel implements ModelInterface {

    /**
     * Metoda koja vraca spisak svih slika, poredjanih po id slike.
     * @return array (Object)
     */
    public static function getAll() {
        $SQL = 'SELECT * FROM `images`;';
        $prep = DataBase::getInstance()->prepare($SQL);
        $prep->execute();
        return $prep->fetchAll(PDO::FETCH_OBJ);
    }

    /**
     * Metoda koja vraca objekat sa podacima slike ciji je image_id,
     * dat kao argument metoda.
     * @return stdClass|NULL
     */
    public static function getById($image_id) {
        $image_id = intval($image_id);
        $SQL = 'SELECT * FROM `images` WHERE `image_id` = ?;';
        $prep = DataBase::getInstance()->prepare($SQL);
        $prep->execute([$image_id]);
        return $prep->fetch(PDO::FETCH_OBJ);
    }
    
    /**
     * Metod koji vraca niz objekata koji su dodeljeni proizvodu u obliku slike. Kao argument metoda,
     * prosledjen je:
     * @param int $product_id
     * @return array
     */
    public static function getImagesByProductId($product_id){
        $product_id = intval($product_id);
        $SQL = 'SELECT * FROM `images` WHERE `product_id` = ?;';
        $prep = DataBase::getInstance()->prepare($SQL);
        $prep->execute([$product_id]);
        return $prep->fetchAll(PDO::FETCH_OBJ);
    }
    
    /**
     * Metod koji vrsi upisivanje u tabelu `images` vezu slike i proizvoda.
     * @param string $path
     * @param int $product_id
     * @return boolean
     */
    public static function add($path, $product_id){
        $SQL = 'INSERT INTO `images` (`path`, `product_id`) VALUES (?, ?);';
        $prep = DataBase::getInstance()->prepare($SQL);
        return $prep->execute([$path, $product_id]);
    }

}
