<?php

/**
 * Model koji je namenjen za rad sa podacima iz tabele `product_packing`.
 */
class PackingModel implements ModelInterface {

    /**
     * Metoda koja vraca spisak pakovanja, poredjanih po imenu.
     * @return array (Object)
     */
    public static function getAll() {
        $SQL = 'SELECT * FROM `product_packing` ORDER By `name`;';
        $prep = DataBase::getInstance()->prepare($SQL);
        $prep->execute();
        return $prep->fetchAll(PDO::FETCH_OBJ);
    }

    /**
     * Metoda koja vraca objekat sa podacima pakovanja ciji je packing_id,
     * dat kao argument metoda.
     * @param int $packing_id
     * @return stdClass|NULL
     */
    public static function getById($packing_id) {
        $packing_id = intval($packing_id);
        $SQL = 'SELECT * FROM `product_packing` WHERE `packing_id`=?;';
        $prep = DataBase::getInstance()->prepare($SQL);
        $prep->execute([$packing_id]);
        return $prep->fetch(PDO::FETCH_OBJ);
    }
    
    /**
     * Metoda preko koje se dodaje novi tip pakovanja proizvoda u tabelu `product_packing`.
     * @param string $name
     * @return boolean|int Ukoliko je zapis uspesno kreiran vratice ID zapisa, a u slucaju da nije
     *                     vratice false.
     */
    public static function add($name) {
        $SQL = 'INSERT INTO `product_packing` (`name`) VALUES (?);';
        $prep = DataBase::getInstance()->prepare($SQL);
        $res = $prep->execute([$name]);
        if ($res) {
            $id = DataBase::getInstance()->lastInsertId();
            return $id;
        } else {
            return false;
        }
    }
    
    /**
     * Metoda koja se koristi za izmenu postojeceg svojstva objekta `packing_id`,
     * gde su kao argumenti funkcije prosledjeni:
     * @param int $packing_id
     * @param string $name
     * @return boolean 
     * @todo Potrebno je implementrirati dodatak, koji ce se odnositi na prikazivanje i sakrivanje
     *       odredjene kategorije. (visible)    
     */
    public static function edit($packing_id, $name) {
        $packing_id = intval($packing_id);
        $SQL = 'UPDATE `product_packing` SET `name` = ? WHERE `packing_id` = ?;';
        $prep = DataBase::getInstance()->prepare($SQL);
        return $prep->execute([$name, $packing_id]);
    }
    

    /**
     * Metod vraca niz objekata sa podacima o pakovanjima koje su dodeljene proizvodu.
     * Kao argument funkcije prosledjen je:
     * @param int $product_id
     * @return array
     */
    public static function getPackingProduct($product_id) {
        $product_id = intval($product_id);
        $SQL = 'SELECT * FROM `product_packing_product` WHERE `product_id`= ?;';
        $prep = DataBase::getInstance()->prepare($SQL);
        $prep->execute([$product_id]);
        $packing = $prep->fetchAll(PDO::FETCH_OBJ);
        $list = [];
        foreach ($packing as $pack) {
            $list[] = ProductModel::getById($pack->packing_id);
        }
        return $list;
    }


}
