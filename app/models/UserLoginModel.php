<?php

/**
 * Model koji je namenjen za rad sa podacima iz tabele `admin_login`.
 */
class UserLoginModel implements ModelInterface {

    /**
     * Metoda koja vraca spisak svih ulogovanih administratora, ulogovanih po datumu.
     * @return array (Object)
     */
    public static function getAll() {
        $SQL = 'SELECT * FROM `admin_login` ORDER By `created_at` DESC;';
        $prep = DataBase::getInstance()->prepare($SQL);
        $prep->execute();
        return $prep->fetchAll(PDO::FETCH_OBJ);
    }

    /**
     * Metoda koja vraca objekat sa podacima administratora ciji je `admin_login_id`,
     * dat kao argument metoda.
     * @param int $admin_login_id
     * @return stdClass|NULL
     */
    public static function getById($admin_login_id) {
        $admin_login_id = intval($admin_login_id);
        $SQL = 'SELECT * FROM `admin_login` WHERE `admin_login_id` = ?;';
        $prep = DataBase::getInstance()->prepare($SQL);
        $prep->execute([$admin_login_id]);
        return $prep->fetch(PDO::FETCH_OBJ);
    }
    
    /**
     * Metod koji vraca spisak prijavljivanja odredjenog korisnika, ciji je $admin_id
     * prosledjen kao argument metoda
     * @param int $admin_id
     * @return array
     */
    public static function getByUserId($admin_id) {
        $admin_id = intval($admin_id);
        $SQL = 'SELECT * FROM `admin_login` ORDER By `created_at` DESC;;';
        $prep = DataBase::getInstance()->prepare($SQL);
        $prep->execute([$admin_id]);
        return $prep->fetchAll(PDO::FETCH_OBJ);
    }


}
