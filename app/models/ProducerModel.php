<?php

/**
 * Model koji je namenjen za rad sa podacima iz tabele `producer_product`.
 */
class ProducerModel implements ModelInterface {

    /**
     * Metoda koja vraca spisak svih proizvodjaca, poredjanih po imenu proizvodjaca.
     * @return array (Object)
     */
    public static function getAll() {
        $SQL = 'SELECT * FROM `producer_product` ORDER by `name`;';
        $prep = DataBase::getInstance()->prepare($SQL);
        $prep->execute();
        return $prep->fetchAll(PDO::FETCH_OBJ);
    }

    /**
     * Metoda koja vraca objekat sa podacima pakovanja ciji je `packing_id`,
     * dat kao argument metoda.
     * @param int $producer_id
     * @return stdClass|NULL
     */
    public static function getById($producer_id) {
        $producer_id = intval($producer_id);
        $SQL = 'SELECT * FROM `producer_product` WHERE `producer_id` = ?;';
        $prep = DataBase::getInstance()->prepare($SQL);
        $prep->execute([$producer_id]);
        return $prep->fetch(PDO::FETCH_OBJ);
    }

    /**
     * Metoda preko koje se dodaju novi proizvodjaci u tabelu `producer_product`.
     * @param string $name
     * @param string $description
     * @return boolean|int Ukoliko je zapis uspesno kreiran vratice ID zapisa, a u slucaju da nije
     *                     vratice false.
     */
    public static function add($name, $description) {
        $SQL = 'INSERT INTO `producer_product` (`name`, `description`) VALUES (?, ?);';
        $prep = DataBase::getInstance()->prepare($SQL);
        $res = $prep->execute([$name, $description]);
        if ($res) {
            $id = DataBase::getInstance()->lastInsertId();
            return $id;
        } else {
            return false;
        }
    }
    
    /**
     * Metoda koja se koristi za izmenu postojeceg svojstva objekta `producer_id`,
     * gde su kao argumenti funkcije prosledjeni:
     * @param int $producer_id
     * @param string $name
     * @param string $description
     * @return boolean 
     * @todo Potrebno je implementrirati dodatak, koji ce se odnositi na prikazivanje i sakrivanje
     *       odredjene kategorije. (visible)    
     */
    public static function edit($producer_id, $name, $description) {
        $producer_id = intval($producer_id);
        $SQL = 'UPDATE `producer_product` SET `name` = ?, `description` = ? WHERE `producer_id` = ?;';
        $prep = DataBase::getInstance()->prepare($SQL);
        return $prep->execute([$name, $description, $producer_id]);
    }

}
