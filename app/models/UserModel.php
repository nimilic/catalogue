<?php

/**
 * Model koji je namenjen za rad sa podacima iz tabele `admin`.
 */
class UserModel implements ModelInterface {

    /**
     * Metoda koja vraca spisak svih administatora, poredjanih po korisnickom imenu.
     * @return array (Object)
     */
    public static function getAll() {
        $SQL = 'SELECT * FROM `admin` ORDER By `username`;';
        $prep = DataBase::getInstance()->prepare($SQL);
        $prep->execute();
        return $prep->fetchAll(PDO::FETCH_OBJ);
    }

    /**
     * Metoda koja vraca objekat sa podacima administratora ciji je admin_id,
     * dat kao argument metoda.
     * @param int $admin_id
     * @return stdClass|NULL
     */
    public static function getById($admin_id) {
        $SQL = 'SELECT * FROM `admin` WHERE `admin_id` = ?;';
        $prep = DataBase::getInstance()->prepare($SQL);
        $prep->execute([$admin_id]);
        return $prep->fetch(PDO::FETCH_OBJ);
    }

    /**
     * Metoda koji vraca objekat sa podacima korisnika koji su prosledjeni kao argumenti funkcije.
     * @param string $username
     * @param string $passwordHash
     * @return stdClass|NULL
     */
    public static function getActiveUserByUsernameAndPasswordHesh($username, $passwordHash) {
        $SQL = 'SELECT * FROM `admin` WHERE `username` = ? and `password_hash` = ? and `active` = 1;';
        $prep = DataBase::getInstance()->prepare($SQL);
        $prep->execute([$username, $passwordHash]);
        return $prep->fetch(PDO::FETCH_OBJ);
    }

    /**
     * Metoda koja vraca objekat sa podatkom korisnika, koji je prosledjen kao argument.
     * @param int $admin_id
     * @return stdClass|NULL
     */
    public static function getActiveUser($admin_id) {
        $SQL = 'SELECT `username` FROM `admin` WHERE `admin_id` = ?;';
        $prep = DataBase::getInstance()->prepare($SQL);
        $prep->execute([$admin_id]);
        return $prep->fetch(PDO::FETCH_OBJ);
    }

}
