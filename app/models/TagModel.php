<?php

/**
 * Model koji je namenjen za rad sa podacima iz tabele `product_tag`.
 */
class TagModel implements ModelInterface {

    /**
     * Metoda koja vraca spisak svih kljucnih reci, poredjanih po imenu.
     * @return array (Object)
     */
    public static function getAll() {
        $SQL = 'SELECT * FROM `product_tag` ORDER by `name`;';
        $prep = DataBase::getInstance()->prepare($SQL);
        $prep->execute();
        return $prep->fetchAll(PDO::FETCH_OBJ);
    }

    /**
     * Metoda koja vraca objekat sa podacima pakovanja ciji je `product_tag_id`,
     * dat kao argument metoda.
     * @param int $product_tag_id
     * @return stdClass|NULL
     */
    public static function getById($product_tag_id) {
        $product_tag_id = intval($product_tag_id);
        $SQL = 'SELECT * FROM `product_tag` WHERE `product_tag_id` = ?;';
        $prep = DataBase::getInstance()->prepare($SQL);
        $prep->execute([$product_tag_id]);
        return $prep->fetch(PDO::FETCH_OBJ);
    }

    /**
     * Metoda preko koje se dodaje novi tip pakovanja proizvoda u tabelu `product_tag`.
     * @param string $name
     * @param string $image_class
     * @return int|boolean
     */
    public static function add($name, $image_class) {
        $SQL = 'INSERT INTO `product_tag` (`name`,`image_class`) VALUES (?, ?);';
        $prep = DataBase::getInstance()->prepare($SQL);
        $res = $prep->execute([$name, $image_class]);
        if ($res) {
            $id = DataBase::getInstance()->lastInsertId();
            return $id;
        } else {
            return false;
        }
    }

    /**
     * Metoda koja se koristi za izmenu postojeceg svojstva objekta `product_tag_id`,
     * gde su kao argumenti funkcije prosledjeni:
     * @param int $product_tag_id
     * @param string $name
     * @param string $image_class
     * @return boolean 
     * @todo Potrebno je implementrirati dodatak, koji ce se odnositi na prikazivanje i sakrivanje
     *       odredjene kategorije. (visible)    
     */
    public static function edit($product_tag_id, $name, $image_class) {
        $product_tag_id = intval($product_tag_id);
        $SQL = 'UPDATE `product_tag` SET `name` = ?, `image_class` = ? WHERE `product_tag_id` = ?;';
        $prep = DataBase::getInstance()->prepare($SQL);
        return $prep->execute([$name, $image_class, $product_tag_id]);
    }

    /**
     * Metod vraca niz objekata sa podacima tagovima koji su dodeljeni proizvodu.
     * Kao argument funkcije prosledjen je:
     * @param int $product_id
     * @return array
     */
    public static function getTagProduct($product_id) {
        $product_id = intval($product_id);
        $SQL = 'SELECT * FROM `product_product_tag` WHERE `product_id` =?;';
        $prep = DataBase::getInstance()->prepare($SQL);
        $prep->execute([$product_id]);
        $tags = $prep->fetchAll(PDO::FETCH_OBJ);
        $list = [];
        foreach ($tags as $tag) {
            $list[] = ProductModel::getById($tag->product_tag_id);
        }
        return $list;
    }

}
