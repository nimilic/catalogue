<?php

/**
 * Model koji je namenjen za rad sa podacima iz tabele `product`.
 */
class ProductModel implements ModelInterface {

    /**
     * Metoda koja vraca spisak svih proizvoda, poredjanih po imenu proizvoda.
     * @return array (Object)
     */
    public static function getAll() {
        $SQL = 'SELECT * FROM `product` ORDER by `name`;';
        $prep = DataBase::getInstance()->prepare($SQL);
        $prep->execute();
        return $prep->fetchAll(PDO::FETCH_OBJ);
    }

    #Paginacija za stranice i proizvod.
    /**
     * Metod koji se koristi za prikaz prozvoda, sa ogranicenjem u prikazu proizvoda po stranici.
     * @param int $page
     * @return array
     */
    public static function getAllPage($page) {
        $page = max(0, $page); #minimalan broj stranica i maksimalan
        $first = $page * Configuration::ITEMS_PER_PAGE;
        $SQL = 'SELECT * FROM `product` ORDER by `name` LIMIT ?, ?;';
        $prep = DataBase::getInstance()->prepare($SQL);
        $prep->execute([$first, Configuration::ITEMS_PER_PAGE]);
        return $prep->fetchAll(PDO::FETCH_OBJ);
    }

    #Paginacija za home
    /**
     * Metod koji se koristi za prikaz najnovijih proizvoda.
     * @param int $page
     * @return array
     */
    public static function getNewProduct($page) {
        $page = max(0, $page); #minimalan broj stranica i maksimalan
        $first = $page * Configuration::ITEMS_NEW_PER_PAGE;
        $SQL = 'SELECT * FROM `product` ORDER by `created_at` DESC LIMIT ?, ?;';
        $prep = DataBase::getInstance()->prepare($SQL);
        $prep->execute([$first, Configuration::ITEMS_NEW_PER_PAGE]);
        return $prep->fetchAll(PDO::FETCH_OBJ);
    }
    /**
     * Metoda koja vraca objekat sa podacima proizvoda ciji je product_id,
     * dat kao argument metoda.
     * @param int $product_id
     * @return stdClass|NULL
     */
    public static function getById($product_id) {
        $product_id = intval($product_id);
        $SQL = 'SELECT * FROM `product` WHERE `product_id` = ?;';
        $prep = DataBase::getInstance()->prepare($SQL);
        $prep->execute([$product_id]);
        return $prep->fetch(PDO::FETCH_OBJ);
    }

    // Ovde sam dodao funkciju za uzimanje korinik
    public static function getByUserId($admin_id) {//
        $admin_id = intval($admin_id); //
        $SQL = 'SELECT * FROM `product` WHERE `admin_id` = ?;'; //
        $prep = DataBase::getInstance()->prepare($SQL);
        $prep->execute([$admin_id]); //
        return $prep->fetchAll(PDO::FETCH_OBJ);
    }

    /**
     * Metoda preko koje se dodaju novi prozivodi u tabelu `product`. 
     * Kao argumenti metoda prosledjeni su sledeci parametri:
     * @param string $name
     * @param string $slug
     * @param string $short_description
     * @param string $long_description
     * @param double $price
     * @param int $product_category_id
     * @param int $producer_id
     * @param int $admin_id
     * @return int|boolean -> Ako je zapis uspesno kreiran vratice ID zapisa, 
     *                        a u slucaju da nije vratice false. (Dodavanje nije uspelo)
     */
    public static function add($name, $slug, $short_description, $long_description, $price, $product_category_id, $producer_id, $admin_id) {
        $SQL = 'INSERT INTO `product`(`name`, `slug`, `short_description`, `long_description`, `price`, `product_category_id`, `producer_id`, `admin_id`) VALUES (?, ?, ?, ?, ?, ?, ?, ?);';
        $prep = DataBase::getInstance()->prepare($SQL);
        $res = $prep->execute([$name, $slug, $short_description, $long_description, $price, $product_category_id, $producer_id, $admin_id]);
        if ($res) {
            $id = DataBase::getInstance()->lastInsertId();
            return $id;
        } else {
            return false;
        }
    }

    /**
     * Metoda koja se koristi za izmenu postojeceg svojstva objekta `product_id`.
     * Kao argumeti metoda prosledjeni su:
     * @param int $product_id
     * @param string $name
     * @param string $slug
     * @param string $short_description
     * @param string $long_description
     * @param double $price
     * @param int $product_category_id
     * @return boolean
     */
    public static function edit($product_id, $name, $slug, $short_description, $long_description, $price, $product_category_id, $producer_id) {
        $product_id = intval($product_id);
        $SQL = 'UPDATE `product` SET `name` = ?, `slug` = ?, `short_description` = ?, `long_description` = ?, `price` = ?, `product_category_id` = ?, `producer_id` = ?  WHERE product_id = ?;';
        $prep = DataBase::getInstance()->prepare($SQL);
        return $prep->execute([$name, $slug, $short_description, $long_description, $price, $product_category_id, $producer_id, $product_id]);
    }

    /**
     * Metoda koji se koristi da brisanje objekta, gde je kao argument metode prosledjen `product_id`
     * @param int $product_id
     * @return boolean -> Ako je objekat uspesno obrisan vratice true, 
     *                    u suprotnom vratice false.
     */
    public static function delete($product_id) {
        $product_id = intval($product_id);
        $SQL = 'DELETE FROM `product` WHERE product_id = ?;';
        $prep = DataBase::getInstance()->prepare($SQL);
        return $prep->execute([$product_id]);
    }

    /**
     * Metoda koja vraca niz objekata sa podacima o tipu pakovanja koje je
     * dodeljeno odredjenom prozovodu, ciji je ID prosledjen kao argument metoda.
     * @param type $product_id
     * @return array
     */
    public static function getProductPacking($product_id) {
        $product_id = intval($product_id);
        $SQL = 'SELECT * FROM `product_packing_product` WHERE `product_id` = ?;';
        $prep = DataBase::getInstance()->prepare($SQL);
        $prep->execute([$product_id]);
        $products = $prep->fetchAll(PDO::FETCH_OBJ);
        $list = [];
        foreach ($products as $product) {
            $list[] = PackingModel::getById($product->packing_id);
        }
        return $list;
    }

    /**
     * Metod se koristi za kreiranje zapisa u veznoj tabeli, id proizovoda i id pakovanja,
     * ovakvih parova moze biti vise.
     * @param int $product_id
     * @param int $packing_id
     * @return boolean
     */
    public static function addProductPacking($product_id, $packing_id) {
        $SQL = 'INSERT IGNORE INTO `product_packing_product`(product_id, packing_id) VALUES (?, ?);';
        $prep = DataBase::getInstance()->prepare($SQL);
        return $prep->execute([$product_id, $packing_id]);
    }

    /**
     * Brise sve kategorije koje su dodeljene ovom proizvodu.
     * @param int $product_id
     * @return boolean | int
     */
    public static function deleteAllPacking($product_id) {
        $SQL = 'DELETE FROM `product_packing_product` WHERE `product_id` = ?;';
        $prep = DataBase::getInstance()->prepare($SQL);
        return $prep->execute([$product_id]);
    }

    /**
     * Metoda koja vraca niz objekata sa podacima o tagocima koji su
     * dodeljeni odredjenom prozovodu, ciji je ID prosledjen kao argument metoda.
     * @param int $product_id
     * @returnarray
     */
    public static function getProductTag($product_id) {
        $product_id = intval($product_id);
        $SQL = 'SELECT * FROM `product_product_tag` WHERE `product_id` = ?;';
        $prep = DataBase::getInstance()->prepare($SQL);
        $prep->execute([$product_id]);
        $products = $prep->fetchAll(PDO::FETCH_OBJ);
        $list = [];
        foreach ($products as $product) {
            $list[] = TagModel::getById($product->product_tag_id);
        }
        return $list;
    }

    /**
     * Metod se koristi za kreiranje zapisa u veznoj tabeli, id proizovoda i id taga,
     * ovakvih parova moze biti vise.
     * @param int $product_id
     * @param int $product_tag_id
     * @return boolean
     */
    public static function addProductTag($product_id, $product_tag_id) {
        $SQL = 'INSERT IGNORE INTO `product_product_tag`(product_id, product_tag_id) VALUES (?, ?);';
        $prep = DataBase::getInstance()->prepare($SQL);
        return $prep->execute([$product_id, $product_tag_id]);
    }

    /**
     * Brise sve kategorije koje su dodeljene ovom proizvodu.
     * @param int $product_id
     * @return boolean | int
     */
    public static function deleteAllTags($product_id) {
        $SQL = 'DELETE FROM `product_product_tag` WHERE `product_id` = ?;';
        $prep = DataBase::getInstance()->prepare($SQL);
        return $prep->execute([$product_id]);
    }
    
    /**
     * Metoda koja vraca niz objekata sa podacima prozivoda koji spadaju pod kategoriju ciji je id prosledjen kao argument..
     * @param int product_category_id
     * @return array (Object)
     */
    public static function getProductCategoryId($product_category_id) {
        $SQL = 'SELECT * FROM `product` WHERE `product_category_id` = ? ORDER By `name`;';
        $prep = DataBase::getInstance()->prepare($SQL);
        $prep->execute([$product_category_id]);
        return $prep->fetchAll(PDO::FETCH_OBJ);
    }
    
    /**
     * Metod koji vraca spisak slika za odredjeni proizvod, gde je kao argument metoda prosledjen:
     * @param int $product_id
     * @return array
     */
    public static function getProductImage($product_id){
        $SQL = 'SELECT * FROM `images` WHERE `product_id` = ?;';
        $prep = DataBase::getInstance()->prepare($SQL);
        $prep->execute([$product_id]);
        return $prep->fetchAll(PDO::FETCH_OBJ);
    }

    /**
     * Metoda koja vraca objekat sa podacim o proizvodu, gde je kao argument metoda prosledjen:
     * @param string $slug
     * @return stdClass
     */
    public static function getByProductSlug($slug){
        $SQL = 'SELECT * FROM `product` WHERE `slug` = ?;';
        $prep = DataBase::getInstance()->prepare($SQL);
        $prep->execute([$slug]);
        return $prep->fetch(PDO::FETCH_OBJ);
    }

    public static function getPackingForProduct($product_id){
        $SQL = 'SELECT * FROM `product_packing_product` WHERE `product_id` = ?;';
        $prep = DataBase::getInstance()->prepare($SQL);
        $prep->execute([$product_id]);
        $packings = $prep->fetchAll(PDO::FETCH_OBJ);
        $list = [];
        foreach ($packings as $pack){
            $list[] = PackingModel::getById($pack->packing_id);
        }
        return $list;
    }
}
