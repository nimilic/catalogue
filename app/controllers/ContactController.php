<?php

class ContactController extends Controller {

    public function index() {
        $this->setData('seo_title', 'Kontakt');
    }

    public function handle() {
        if ($_POST) {
            $email = filter_input(INPUT_POST, 'email', FILTER_SANITIZE_EMAIL);
            $subject = filter_input(INPUT_POST, 'subject');
            $message = filter_input(INPUT_POST, 'message');

            $result = ContactModel::insert($email, $subject, $message);
            if ($result) {
                $this->setData('message', 'Uspesno ste poslali poruku!');
            } else {
                $this->setData('message', 'Doslo je do greske pokusajte kasnije ponovo!');
            }
        }else {
            $this->setData('message', 'Doslo je dao neocekivane greske!');
        }
    }

}
