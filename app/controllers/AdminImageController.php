<?php
/*
 * Klasa admin kontrolera koja omogucava upravljanje fotografijama koje je potrebno dodeliti proizvodu.
 */
class AdminImageController extends AdminController {
    
    public function index() {
        
    }
    
    /**
     * Metod ima zadatak da vrati prikaze listu slika koje pripadaju konkretom proizvodu.
     * @param int $product_id
     */
    public function listProductImages($product_id){
        $this->setData('images', ImagesModel::getImagesByProductId($product_id));
        $this->setData('product_id', $product_id);
    }
    
    public function uploadImage($product_id){
        $this->setData('product_id', $product_id);
        if(!$_FILES or !isset($_FILES['image'])) return;
        
        if($_FILES['image']['error'] != 0){
            $this->setData('message', 'Doslo je do greske prilikom dodavanja fotografije! 1');
        }
        
        $temporaryPath = $_FILES['image']['tmp_name'];
        $fileSize      = $_FILES['image']['size'];
        $originalName  = $_FILES['image']['name'];
        
        if($fileSize > 400 * 1024){
            $this->setData('message', 'Nije moguce dodati fotografiju vecu od 300KB. 2');
        }        
        
        $finfo = new finfo(FILEINFO_MIME_TYPE);
        $mimeType = $finfo->file($originalName);
        
        if($mimeType != 'image/jpeg'){
            $this->setData('message', 'Odabrali ste neadekvatan tip fajla za dodavanje. 3');
        }
        
        $basedName = basename($originalName);
        $lowerCaseName = strtolower($basedName);
        $filteredName = preg_replace('/[^a-z0-9\- ]/', '', $lowerCaseName);
        $spaceToDashName = preg_replace('/[\ ]+/', '-', $filteredName);
        $path = preg_replace('/[\-]+/', '-', $spaceToDashName) . '.jpg';            
                
        $newLocation = Configuration::IMAGE_PATH . $path;
        
        $result = move_uploaded_file($temporaryPath, $newLocation);
        
        if(!$result){
            $this->setData('message', 'Nemate pravo pristupa ovom direktorijumu! 4');
            return;
        }
        
        $res = ImagesModel::add($newLocation, $product_id);
 
        if(!$res){
            $this->setData('message', 'Doslo je do greske u dodavanju slike. 5');
            return;
        }
        Misc::redirect('images/product/' . $product_id);
    }
}
