<?php

/*
 * Klasa admin kontrolera koja omogucava upravljanje proizvodjacima proizvoda koji
 * ce biti dodeljivani razlicitim proizvodima.
 */

class AdminProducerController extends AdminController {

    /**
     * Osnovni metod klase, koji je zaduzen za prikaz
     * svih proizvodjaca iz modela.
     * @see ProducerModel -> function getAll();
     * <pre><code>
     * $SQL = 'SELECT * FROM `product_producer` ORDER By `name`;';
     * </code></pre>
     */
    public function index() {
        $producers = ProducerModel::getAll();
        $this->setData('producers', $producers);
        $this->setData('seo_title', 'Lista proizvodjača');
    }

    /**
     * Metoda koja se koristi za proveru poslatih podataka za dodavanje preko $_POST-a.
     * @see  ProducerModel -> public static function add($name){..}
     * @return void 
     */
    public function add() {
        if ($_POST) {
            $name = filter_input(INPUT_POST, 'producer', FILTER_SANITIZE_STRING);
            $description = filter_input(INPUT_POST, 'description', FILTER_SANITIZE_STRING);
            /**
             * Provera da li postoji odredjeni rekord u tabeli `produser_product`.
             * @return boolean (false) ukoliko pronadje poklapanje
             */
            $produsers = ProducerModel::getAll();
            foreach ($produsers as $producer) {
                if ($producer->name == $name) {
                    $this->setData('message', 'Zapis pod imenom "' . $name . '" je već kreiran.');
                    return false;
                }
            }

            if ($name == '' || $description == '') {
                $this->setData('message', 'Neispravna forma unosa.');
            } else {
                ProducerModel::add($name, $description);
                Misc::redirect('producer/list');
            }
        }
        $this->setData('seo_title', 'Kreiranje proizvodjača');
    }

    /**
     * Metoda koja se koristi za izmenu ili vrsenje izmene podataka
     * poslatih podataka za dodavanje preko $_POST-a.
     * @see ProduserModel -> public static function edit($product_category_id, $name);
     * @param int $producer_id
     * @return void
     */
    public function edit($producer_id) {
        /**
         * Ucitavanje svakog proizvodjaca posebno kako bi se omogucila izmena. (ID se uzima.)
         */
        $producers = ProducerModel::getById($producer_id);
        $this->setData('produser', $producers);
        $this->setData('seo_title', 'Izmena proizvodjača');

        if ($_POST) {
            $name = filter_input(INPUT_POST, 'producer', FILTER_SANITIZE_STRING);
            $description = filter_input(INPUT_POST, 'description', FILTER_SANITIZE_STRING);
            /**
             * Provera da li postoji odredjeni rekord u tabeli `product_producer`.
             * @return boolean (false) ukoliko pronadje poklapanje
             */
            $produsers = ProducerModel::getAll();
            foreach ($produsers as $producer) {
                if ($producer->name == $name) {
                    $this->setData('message', 'Zapis pod imenom "' . $name . '" je već kreiran.');
                    return false;
                }
            }

            if ($name == '' || $description == '') {
                $this->setData('message', 'Neispravna forma unosa.');
            } else {
                ProducerModel::edit($producer_id, $name, $description);
                Misc::redirect('producer/list');
            }
        }
        $this->setData('seo_title', 'Izmena proizvođača');
    }

}
