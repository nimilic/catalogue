<?php

/*
 * Klasa admin kontrolera koja omogucava upravljanje kategorijama.
 */
class AdminCategoryController extends AdminController {

    /**
     * Osnovni metod klase, koji je zaduzen za prikaz svih kategorija iz modela.
     * @see CategoryModel -> function getAll();
     * <pre><code>
     * $SQL = 'SELECT * FROM `product_category` ORDER By `name`;';
     * </code></pre>
     */
    public function index() {
        $categories = CategoryModel::getAll();
        $this->setData('categories', $categories);
        $this->setData('seo_title', 'Lista Kategorija');
    }

    /**
     * Metoda koja se koristi za proveru poslatih podataka za dodavanje preko $_POST-a.
     * @see  CategoryModel -> public static function add($name){..}
     * @return void 
     * @todo Ostalo je da se implementira opcija visible, koja ce omoguciti da se odredjena
     *       kategorija po potrebi prikaze ili sakrije.
     */
    public function add() {
        if ($_POST) {
            $name = filter_input(INPUT_POST, 'name', FILTER_SANITIZE_STRING);
            $origName = $name;
            /**
             * Provera da li postoji odredjeni rekord u tabeli `product_category`.
             * @return boolean (false) ukoliko pronadje poklapanje
             */
            $categories = CategoryModel::getAll();
            foreach ($categories as $category) {
                if ($category->name == $name) {
                    $this->setData('message', 'Zapis pod imenom "' . $name . '" je već kreiran.');
                    return false;
                }
            }
            
            $baseName = basename($origName);
            $lowerCaseSlug = strtolower($baseName);
            $slug = preg_replace('/([\ ]+)/', '-', $lowerCaseSlug);
            
            if (preg_match('/[A-Z][A-z\ ]+)/', $name) or $slug == '' ) {
                $this->setData('message', 'Neispravna forma unosa.');
            } else {
                CategoryModel::add($name, $slug);
                Misc::redirect('category/list');
            }
        }
        $this->setData('seo_title', 'Dodavanje Kategorije');
    }

    /**
     * Metoda koja se koristi za izmenu ili vrsenje izmene podataka
     * poslatih podataka za dodavanje preko $_POST-a.
     * @see  AdminCategory -> public static function edit($name){..}
     * @param type $product_category_id
     * @return void 
     * @todo Potrebno je implementirati nacin za uzimanje zapisa iz baze koji se odnosi na
     *       na lozinku i da se omoguci azuriranje iste.
     */
    public function edit($product_category_id) {
        /**
         * Ucitavanje svake kategorije posebno kako bi se omogucila izmena. (ID se uzima.)
         */
        $category = CategoryModel::getById($product_category_id);
        $this->setData('category', $category);
        if ($_POST) {
            $name = filter_input(INPUT_POST, 'name', FILTER_SANITIZE_STRING);
            $origName = $name;
            /**
             * Provera da li postoji odredjeni rekord u tabeli `product_category`.
             * @return boolean (false) ukoliko pronadje poklapanje
             */
            $categories = CategoryModel::getAll();
            foreach ($categories as $category) {
                if ($category->name == $name) {
                    $this->setData('message', 'Zapis pod imenom "' . $name . '" je već kreiran.');
                    return $this->setData('category', $category);
                }
            }
            
            $baseName = basename($origName);
            $lowerCaseSlug = strtolower($baseName);
            $slug = preg_replace('/([\ ]+)/', '-', $lowerCaseSlug);
            
            if (preg_match('/^[A-Z][A-z\ ]+)$/', $name)) {
                $this->setData('message', 'Neispravna forma unosa.');
            } else {
                CategoryModel::edit($product_category_id, $name, $slug);
                Misc::redirect('category/list');
            }
        }
        $this->setData('seo_title', 'Izmena Kategorija');
    }

}
