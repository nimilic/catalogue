<?php

class MapsController extends Controller {

    public function index() {
        $this->setData('seo_title', 'Mapa');
        $this->setData('subject', 'Da li smo na mapi?!');
        $this->setData('subtitle', 'Radno vreme');
        $this->setData('description', 'Ukoliko imate vremena i volje možete nas lično posetiti i još bliže se informisati '
                        . 'o pojedinim proizvodima i o ostalim potrebštinama.<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Naše radno vreme je: ');       
        $this->setData('workday', 'Radnim danima: &nbsp;&nbsp;&nbsp;od: 8:00h do: 19:00h');
        $this->setData('weekend', 'Subotom: &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'
                        . '&nbsp;&nbsp;&nbsp;&nbsp;od: 8:00h do: 14:00h');
        
        $this->setData('maps', 'Ukoliko imate problema da nas pronađete, mislili smo i o tome:');
    }

}
