<?php

/**
 * Ovo je osnovni kontroler aplikacije koji se koristi za izvrsavanje
 * zahteva upucenih prema podrazumevanim rutama koje poznaje veb sajt.
 */
class MainController extends Controller {

    /**
     * Osnovni metod, pocetne stranice sajta.
     */
    public function index($page = 0) {
        $this->setData('seo_title', 'Početna');
                  
        $products = ProductModel::getNewProduct($page);
        for ($i = 0; $i < count($products); $i++){
            $products[$i]->images = ProductModel::getProductImage($products[$i]->product_id);
        }       
        $this->setData('products', $products);
    }  
    
    public function home($seo_url){
        $activeAdmin = UserModel::getActiveUser(Session::getKey('admin_id'), UserModel::getAll());
        $this->setData('admins', $activeAdmin); 
        $page = PageModel::getBySeoUrl($seo_url);
        if (!$page) {
            $this->setData('page', Misc::redirect('page/error/'));
        }
        $this->setData('seo_title', $page->seo_title);
        $this->setData('page', $page);
    }

    /**
     * Ovaj metod proverava da li postoje podaci za prijavu poslati HTTP POST
     * metodom, vrsi njihovu validaciju, proverava postojanje korisnika sa tim
     * pristupnim parametrima i u slucaju da sve provere prodju bez greske
     * metod kreira sesiju za korisnika i preusemrava korisnika na default rutu.
     * @return void Metod ne vraca nista, vec koristi return naredbu za prekid izvrsavanja u odredjenim situacijama
     */
    public function login() {
        if ($_POST) {
            $username = filter_input(INPUT_POST, 'username');
            $password = filter_input(INPUT_POST, 'password'); //

            if (preg_match('/^[a-z0-9]{4,}$/', $username) && preg_match('/^.{6,}$/', $password)) {
                $passwordHash = hash('sha512', $password . Configuration::SALT);
                $user = UserModel::getActiveUserByUsernameAndPasswordHesh($username, $passwordHash);
                if ($user) {
                    Session::set('admin_id', $user->admin_id);
                    Session::set('username', $username);
//                    Session::set('user_ip', filter_input(INPUT_SERVER, 'REMOTE_ADDR', FILTER_FLAG_IPV4));
                    Misc::redirect('admin/home');
                } else {
                    $this->setData('message', 'Username or password are incorect, or user is not active!');
                }
            } else {
                $this->setData('message', 'Invalid username or password are incorect!');
            }
        }
        $this->setData('seo_title', 'Login');
        $this->setData('text', 'Ukoliko želite da pristupite panelu za administratore molimo Vas da popunite '
                . 'formular za prijavu ispod teksta!');
    }

    /**
     * Ovaj metod gasi sesiju cime efektivno unistava sve u sesiji,
     * a zatim preusmerava korisnika na stranicu za prijavu na login ruti.
     */
    public function logout() {
        Session::end();
        #Redirekcija na pocetnu stranicu!
        Misc::redirect('');
    }
    

}
