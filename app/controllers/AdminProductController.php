<?php

/**
 * Klasa koja omogucava upravljanje proizvodima koji ce biti prikazani.
 */
class AdminProductController extends AdminController {

    /**
     * * Osnovni metod klase, koji je zaduzen za prikaz svih proizvoda iz modela.
     */
    public function index() {
        $products = ProductModel::getByUserId(Session::getKey('admin_id'));

        $categories = CategoryModel::getAll();
        $this->setData('categories', $categories);

        for ($i = 0; $i < count($products); $i++) {
            $products[$i]->packing = ProductModel::getProductPacking($products[$i]->product_id);
            $products[$i]->tags = ProductModel::getProductTag($products[$i]->product_id);
        }
        $this->setData('products', $products);
        $this->setData('seo_title', 'Lista Proizvoda');
    }

    /**
     * Metoda koja se koristi za proveru poslatih podataka za dodavanje preko $_POST-a.
     * @return void 
     */
    public function add() {
        $this->setData('categories', CategoryModel::getAll());
        $this->setData('packing', PackingModel::getAll());
        $this->setData('tags', TagModel::getAll());
        $this->setData('producers', ProducerModel::getAll());

        if ($_POST) {
            $name = filter_input(INPUT_POST, 'name', FILTER_SANITIZE_STRING);
            # Uzeto ime iz imena porizvoda, kako korisnik ne bi morao da ponovo ukucava,
            # ime u izmenjenom obliku.
            $slug_tmp = $name;
            $short_description = filter_input(INPUT_POST, 'short_description', FILTER_SANITIZE_STRING);
            $long_description = filter_input(INPUT_POST, 'long_description', FILTER_SANITIZE_STRING);
            $price = floatval(filter_input(INPUT_POST, 'price', FILTER_SANITIZE_NUMBER_FLOAT));

            $category = filter_input(INPUT_POST, 'category_id', FILTER_SANITIZE_NUMBER_INT);

            $packing = filter_input(INPUT_POST, 'packing', FILTER_DEFAULT, FILTER_REQUIRE_ARRAY);
            $tags = filter_input(INPUT_POST, 'tag', FILTER_DEFAULT, FILTER_REQUIRE_ARRAY);

            $producer = filter_input(INPUT_POST, 'producer_id', FILTER_SANITIZE_NUMBER_INT);

            # Prilagodjavanje za slug.
            $baseName = basename($slug_tmp);
            $slugLowerCase = strtolower($baseName);
            $slugSpeace = preg_replace('/[\ ]+/', '-', $slugLowerCase);
            $slug = $slugSpeace;

            if ($name == '' or $slug == '' or $short_description == '' or $long_description == '' or $price == 0) {
                $this->setData('message', 'Incorrect values submitted');
            } elseif (strlen($short_description) > 400) {
                $this->setData('message', 'Kratak opis prozvoda ne može da sadrži više od 400 karaktera.');
            } else {
                $id = ProductModel::add($name, $slug, $short_description, $long_description, $price, $category, $producer, Session::getKey('admin_id'));
                if ($id) {
                    foreach ($packing as $pack) {
                        ProductModel::addProductPacking($id, $pack);
                    }
                    foreach ($tags as $tag) {
                        ProductModel::addProductTag($id, $tag);
                    }
                    Misc::redirect('product/list');
                } else {
                    $this->setData('message', 'Could not add new product!');
                }
            }
        }
        $this->setData('seo_title', 'Dodavanje Proizvoda');
    }

    /**
     * Metoda koja se koristi za izmenu ili vrsenje izmene podataka
     * poslatih podataka za dodavanje preko $_POST-a.
     * @param int $product_id
     * @return void
     */
    public function edit($product_id) {
        $this->setData('categories', CategoryModel::getAll());
        $this->setData('packing', PackingModel::getAll());
        $this->setData('tags', TagModel::getAll());
        $this->setData('producers', ProducerModel::getAll());

        $product = ProductModel::getById($product_id);
        $product->packing = ProductModel::getProductPacking($product_id);
        $product->packings_id = [];
        foreach ($product->packing as $pack) {
            $product->packings_id[] = $pack->packing_id;
        }
        $product->tags = ProductModel::getProductTag($product_id);
        $product->tags_id = [];
        foreach ($product->tags as $tag) {
            $product->tags_id[] = $tag->product_tag_id;
        }
        $this->setData('product', $product);


        if ($product->admin_id != Session::getKey('admin_id')) {
            Misc::redirect('product/list');
        }
        if ($_POST) {
            $name = filter_input(INPUT_POST, 'name', FILTER_SANITIZE_STRING);
            # Uzeto ime iz imena porizvoda, kako korisnik ne bi morao da ponovo ukucava,
            # ime u izmenjenom obliku.
            $slug_tmp = $name;
            $short_description = filter_input(INPUT_POST, 'short_description', FILTER_SANITIZE_STRING);
            $long_description = filter_input(INPUT_POST, 'long_description', FILTER_SANITIZE_STRING);
            $price = floatval(filter_input(INPUT_POST, 'price', FILTER_SANITIZE_NUMBER_FLOAT));
            #Dobija niz preko inputa!
            $category = filter_input(INPUT_POST, 'category_id', FILTER_SANITIZE_NUMBER_INT);

            $packing = filter_input(INPUT_POST, 'packings_id', FILTER_SANITIZE_NUMBER_INT, FILTER_REQUIRE_ARRAY);
            $tags = filter_input(INPUT_POST, 'tags_id', FILTER_SANITIZE_NUMBER_INT, FILTER_REQUIRE_ARRAY);

            $producer = filter_input(INPUT_POST, 'producer_id', FILTER_SANITIZE_NUMBER_INT);

            $baseName = basename($slug_tmp);
            $slugLowerCase = strtolower($baseName);
            $slugSpeace = preg_replace('/[\ ]+/', '-', $slugLowerCase);
            $slug = $slugSpeace;

            if ($name == '' or $slug == '' or $short_description == '' or $long_description == '' or $price == 0) {
                $this->setData('message', 'Incorrect values submitted');
            }elseif (strlen($short_description) > 400) {
                $this->setData('message', 'Kratak opis prozvoda ne može da sadrži više od 400 karaktera.');
            } else {
                $id = ProductModel::edit($product_id, $name, $slug, $short_description, $long_description, $price, $category, $producer);
                if ($id) {
                    ProductModel::deleteAllPacking($product_id);
                    foreach ($packing as $pack_id) {
                        ProductModel::addProductPacking($product_id, $pack_id);
                    }
                    ProductModel::deleteAllTags($product_id);
                    foreach ($tags as $tags_id) {
                        ProductModel::addProductTag($product_id, $tags_id);
                    }
                    Misc::redirect('product/list');
                } else {
                    $this->setData('message', 'Could not edit this product!');
                }
            }
        }
        $this->setData('seo_title', 'Izmena Proizvoda');
    }

    /**
     * Metoda se koristi za brisanje proizvoda iz tabele `product`.
     * @param int $product_id
     */
    public function delete($product_id) {
        if ($_POST) {
            $confirmed = filter_input(INPUT_POST, 'confirmed', FILTER_SANITIZE_NUMBER_INT);

            if ($confirmed == 1) {
                $res = ProductModel::delete($product_id);
                if ($res) {
                    Misc::redirect('product/list');
                } else {
                    $this->setData('message', 'Could not delete this product!');
                }
            }
        }
        $product = ProductModel::getById($product_id);
        $this->setData('product', $product);
    }

}
