<?php

class ProductController extends Controller {

    public function index($page = 0) { # Podrazumevana vrednost stranice
        $this->setData('seo_title', 'Proizvodi');
        $this->setData('categories', CategoryModel::getAll()); # Prikaz svih kategorija.
        
        $products = ProductModel::getAllPage($page);
        for ($i = 0; $i < count($products); $i++){
            $products[$i]->images = ProductModel::getProductImage($products[$i]->product_id);
        }       
        $this->setData('products', $products);
    }
           
    /**
     * Metod koji salje view-u spisak proizvoda koji pripadaju odredjenoj kategoriji, 
     * definisanu preko `slug` polja.
     * @param string $categorySlug
     */
    public function listByCategory($categorySlug){
        $category = CategoryModel::getCategoryBySlug($categorySlug); # Prikaz za link.
        
        if(!$category){
            Misc::redirect('');
        }
        $this->setData('categories', CategoryModel::getAll());
        
        $products = ProductModel::getProductCategoryId($category->product_category_id);
        
        for ($i = 0; $i < count($products); $i++){
            $products[$i]->images = ProductModel::getProductImage($products[$i]->product_id);
        }  
        $this->setData('products', $products);
        $this->setData('category', $category);
    }
    
    public function details($slug) {
        $product = ProductModel::getByProductSlug($slug);
        $this->setData('seo_title', 'Detalji');
        
        if(!$product){
            Misc::redirect('');
        }
        
        $this->setData('categories', CategoryModel::getAll());
        
        $product->images = ProductModel::getProductImage($product->product_id);
        $product->producer = ProducerModel::getById($product->producer_id);
        $product->categories = CategoryModel::getById($product->product_category_id);
        $product->packings = ProductModel::getPackingForProduct($product->product_id);
        
        
        $this->setData('product', $product);
    }
}
