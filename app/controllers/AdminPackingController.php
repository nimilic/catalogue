<?php

/*
 * Klasa admin kontrolera koja omogucava upravljanje tipovima pakovanja koji
 * ce biti dodeljivani razlicitim proizvodima.
 */

class AdminPackingController extends AdminController {

    /**
     * Osnovni metod klase, koji je zaduzen za prikaz
     * svih tipova pakovanja iz modela.
     * @see PackingModel -> function getAll();
     * <pre><code>
     * $SQL = 'SELECT * FROM `product_packing` ORDER By `name`;';
     * </code></pre>
     */
    public function index() {
        $packing = PackingModel::getAll();
        $this->setData('packing', $packing);
        $this->setData('seo_title', 'Lista pakovanja');
    }

    /**
     * Metoda koja se koristi za proveru poslatih podataka za dodavanje preko $_POST-a.
     * @see  PackingModel -> public static function add($name){..}
     * @return void 
     */
    public function add() {
        if ($_POST) {
            $name = filter_input(INPUT_POST, 'packing', FILTER_SANITIZE_STRING);
            /**
             * Provera da li postoji odredjeni rekord u tabeli `product_paking`.
             * @return boolean (false) ukoliko pronadje poklapanje
             */
            $packing = PackingModel::getAll();
            foreach ($packing as $pack) {
                if ($pack->name == $name) {
                    $this->setData('message', 'Zapis pod imenom "' . $name . '" je već kreiran.');
                    return false;
                }
            }

            if (preg_match('/^[a-z\ ]+)$/', $name)) {
                $this->setData('message', 'Neispravna forma unosa.');
            } else {
                PackingModel::add($name);
                Misc::redirect('packing/list');
            }
        }
        $this->setData('seo_title', 'Dodavanje pakovanja');
    }

    /**
     * Metoda koja se koristi za izmenu ili vrsenje izmene podataka
     * poslatih podataka za dodavanje preko $_POST-a.
     * @see AdminModel -> public static function edit($product_category_id, $name);
     * @param int $packing_id
     * @return void 
     */
    public function edit($packing_id) {
        /**
         * Ucitavanje svakog pakovanja posebno kako bi se omogucila izmena. (ID se uzima.)
         */
        $packing = PackingModel::getById($packing_id);
        $this->setData('packing', $packing);

        if ($_POST) {
            $name = filter_input(INPUT_POST, 'packing', FILTER_SANITIZE_STRING);
            /**
             * Provera da li postoji odredjeni rekord u tabeli `product_category`.
             * @return boolean (false) ukoliko pronadje poklapanje
             */
            $packing = PackingModel::getAll();
            foreach ($packing as $pack) {
                if ($pack->name == $name) {
                    $this->setData('message', 'Zapis pod imenom "' . $name . '" je već kreiran.');
                    return $this->setData('packing', $pack);
                }
            }

            if (preg_match('/^[a-z]([\ ?[a-z]+)/$', $name)) {
                $this->setData('message', 'Neispravna forma unosa.');
            } else {
                PackingModel::edit($packing_id, $name);
                Misc::redirect('packing/list');
            }
        }
        $this->setData('seo_title', 'Izmena pakovanja');
    }

}
