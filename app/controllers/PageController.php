<?php

class PageController extends Controller {

    public function index() {
        
    }

    public function show($seo_url) {
        $page = PageModel::getBySeoUrl($seo_url);
        if (!$page) {
            $this->setData('page', Misc::redirect('page/error/'));
        }
        $this->setData('seo_title', $page->seo_title);
        $this->setData('page', $page);
    }

    public function error() {
        $this->setData('seo_title', 'Error');
        $this->setData('title', 'Fatal Error!');
        $this->setData('description', 'This content is not available!');
    }
}
