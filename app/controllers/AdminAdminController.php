<?php

/*
 * Klasa admin kontrolera koja omogucava upravljanje administratorima.
 */

class AdminAdminController extends AdminController {

    /**
     * Osnovni metod klase, koji je zaduzen 
     * za prikaz svih administratora iz tabele `admin`.
     * @see AdminModel -> function getAll();
     * <pre><code>
     * $SQL = 'SELECT * FROM `admin` ORDER By `username`;';
     * </code></pre>
     */
    public function index() {
        $admins = AdminModel::getAll();
        $this->setData('admins', $admins);
        $this->setData('seo_title', 'Lista administratora');
    }

    /**
     * Metoda koja se koristi za proveru poslatih podataka
     * koji se koriste za dodavanje preko $_POST metoda.
     * @see  AdminModel -> public static function add($name){..}
     * @return void 
     */
    public function add() {
        $this->setData('seo_title', 'Kreiranje administratora');
        if ($_POST) {

            $username = filter_input(INPUT_POST, 'username', FILTER_SANITIZE_STRING);
            $password_hash = filter_input(INPUT_POST, 'password_hash', FILTER_SANITIZE_STRING);

            if (!preg_match('/^[a-z0-9]{4,}$/', $username) || !preg_match('/^.{6,}$/', $password_hash)) {
                $this->setData('message', 'Nesto je vrong');
            } else {
                AdminModel::add($username, hash('sha512', $password_hash . Configuration::SALT));
                Misc::redirect('admins/list');
            }
        }
    }

    /**
     * Metoda koja se koristi za izmenu ili vrsenje izmene podataka
     * poslatih podataka za dodavanje preko $_POST-a.
     * @see  AdminModel -> public static function edit($name){..}
     * @param int $admin_id
     * @return void 
     * @todo Potrebno je implementirati nacin za uzimanje zapisa iz baze koji se odnosi na
     *       na lozinku i da se omoguci azuriranje iste.
     */
    public function edit($admin_id) {
        $admin = AdminModel::getById($admin_id);
        $this->setData('admin', $admin);
        if ($_POST) {

            $username = filter_input(INPUT_POST, 'username', FILTER_SANITIZE_STRING);
            $password_hash = filter_input(INPUT_POST, 'password_hash', FILTER_SANITIZE_STRING);

            if (!preg_match('/^[a-z0-9]{4,}$/', $username) || !preg_match('/^.{6,}$/', $password_hash)) {
                $this->setData('message', 'Nesto je vrong');
            } else {
                AdminModel::edit($admin_id, $username, hash('sha512', $password_hash . Configuration::SALT));
                Misc::redirect('admins/list');
            }
        }
        $this->setData('seo_title', 'Izmena administratoira');
    }

}
