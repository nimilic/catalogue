<?php

/*
 * Klasa koja omogucava upravljanje tagovima koje
 * ce biti, dodeljivani razlicitim proizvodima.
 */

class AdminTagController extends AdminController {

    /**
     * Osnovni metod klase, koji je zaduzen za prikaz svih tagova iz modela.
     * @see TagModel -> function getAll();
     * <pre><code>
     * $SQL = 'SELECT * FROM `product_tag` ORDER By `name`;';
     * </code></pre>
     */
    public function index() {
        $tags = TagModel::getAll();
        $this->setData('tags', $tags);
        $this->setData('seo_title', 'Lista tagova');
    }

    /**
     * Metoda koja se koristi za proveru poslatih podataka za dodavanje preko $_POST-a.
     * @see  TagModel -> public static function add($name){..}
     * @return void 
     */
    public function add() {
        if ($_POST) {
            $name = filter_input(INPUT_POST, 'name', FILTER_SANITIZE_STRING);
            $image_class = filter_input(INPUT_POST, 'image_class', FILTER_SANITIZE_STRING);
            /**
             * Provera da li postoji odredjeni rekord u tabeli `product_tag`.
             * @return boolean (false) ukoliko pronadje poklapanje
             */
            $tags = TagModel::getAll();
            foreach ($tags as $tag) {
                if ($tag->name == $name) {
                    $this->setData('message', 'Zapis pod imenom "' . $name . '" je već kreiran.');
                    return false;
                }
            }
            if (preg_match('/^[a-z0-9\- ]\+$/', $name) || $image_class == '') {
                $this->setData('message', 'Neispravna forma unosa.');
            } else {
                TagModel::add($name, $image_class);
                Misc::redirect('tag/list');
            }
        }
        $this->setData('seo_title', 'Dodavanje Taga');
    }

    /**
     * Metoda koja se koristi za izmenu ili vrsenje izmene podataka
     * poslatih podataka za dodavanje preko $_POST-a.
     * @see CategoryModel -> public static function edit($product_category_id, $name);
     * @param int $product_tag_id
     * @return boolean
     */
    public function edit($product_tag_id) {
        /**
         * Ucitavanje сваког тага posebno kako bi se omogucila izmena. (ID se uzima.)
         */
        $tags = TagModel::getById($product_tag_id);
        $this->setData('tags', $tags);
        if ($_POST) {
            $name = filter_input(INPUT_POST, 'name', FILTER_SANITIZE_STRING);
            $image_class = filter_input(INPUT_POST, 'image_class', FILTER_SANITIZE_STRING);
            /**
             * Provera da li postoji odredjeni rekord u tabeli `product_tag`.
             * @return boolean (false) ukoliko pronadje poklapanje
             */
            $tags = TagModel::getAll();
            foreach ($tags as $tag) {
                if ($tag->name == $name) {
                    $this->setData('message', 'Zapis pod imenom "' . $name . '" je već kreiran.');
                    return false;
                }
            }
            if (preg_match('/^[a-z0-9\- ]\+$/', $name) || $image_class == '') {
                $this->setData('message', 'Neispravna forma unosa.');
            } else {
                TagModel::edit($product_tag_id, $name, $image_class);
                Misc::redirect('tag/list');
            }
        }
        $this->setData('seo_title', 'Izmena tagova');
    }

}
